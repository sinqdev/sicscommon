source $home/sicscommon/stddrive.tcl
makesctcontroller rosch std boa-prim:3002 \n 2 \n \n
rosch transact *IDN?
rosch transact *IDN?
rosch debug -1

proc freqwrite {} {
    hset /sics/freq/stop 0
    set val [sct target]
    set val [expr int($val)]
    set command [format "SOUR:FREQ %d{0}" $val]
    sct send $command
    return freqempty
}
#----------------------------------------
proc freqempty {} {
    return idle
}
#------------------------------------------
proc freqread {} {
    sct send "SOUR:FREQ?"
    return freqrep
}
#------------------------------------------
proc freqrep  {} {
    set val [string trim [sct result]]
    sct update $val
    return idle
}

stddrive::makestddrive freq ROSCH rosch
freq lowerlimit 0
freq upperlimit 10000000
hsetprop /sics/freq write freqwrite
hsetprop /sics/freq freqempty freqempty
hsetprop /sics/freq read freqread
hsetprop /sics/freq freqreply freqreply


proc powwrite {} {
    hset /sics/pow/stop 0
    set val [sct target]
    set val [expr int($val)]
    set command [format "SOUR:POW %d{0}" $val]
    sct send $command
    return freqempty
}
#------------------------------------------
proc powread {} {
    sct send "SOUR:POW?"
    return freqrep
}

stddrive::makestddrive pow ROSCH rosch
pow lowerlimit -13
pow upperlimit 13
pow tolerance .5
hsetprop /sics/pow write powwrite
hsetprop /sics/pow freqempty freqempty
hsetprop /sics/pow read powread
hsetprop /sics/pow freqreply freqreply
