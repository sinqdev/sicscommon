#---------------------------------------------------
# This is a scriptcontext based driver for the 
# old RS-232 connected Siemens SPS at SINQ. 
# 
# copyright: see file COPYRIGHT
#
# Mark Koennecke, March 2011
#---------------------------------------------------

namespace eval spss5 {} 

proc spss5::sendbytes {} {
    sct send "R"
    return readbytes
}
#--------------------------------------------------
proc spss5::readbytes {} {
    set vals [sct result]
    if {[string first ERR $vals] >= 0 || \
	    [string first AscErr $vals] >=0} {
	clientput "ERROR: $vals reading SPS"
    } else {
	set v [split $vals]
	sct update [lrange $v 1 end]
    }
    return idle
}
#---------------------------------------------------
proc spss5::sendadc {} {
    sct send "A"
    return readadc
}
#---------------------------------------------------
proc spss5::push {sct byte bit} {
    set com [format "S%3.3d%1.1d" $byte $bit]
    $sct transact $com
}
#--------------------------------------------------
proc spss5::adc {name num} {
    set adclist [hval /sics/${name}/adcs]
    return [lindex $adclist $num]
}
#--------------------------------------------------
proc spss5::waitnew {name} {
    set start [doubletime]
    set readtime 0
    ${name}sct queue /sics/${name}/bytes progress read
    while {$start > $readtime} {
	wait 1
	set int [getint] 
	if {[string compare $int continue] != 0} {
	    error interrupt
	}
	set status [catch {SplitReply [hgetprop /sics/${name}/bytes read_time]} readtime]
	if {$status != 0} {
	    set readtime 0
	}
    }
}
#---------------------------------------------------
proc spss5::stat2 {name byte bit} {
    waitnew $name
    set bytelist [hval /sics/${name}/bytes]
    set byte [lindex $bytelist [expr $byte -1]]
    set bitval [expr $byte & 1 << $bit]
    if {$bitval > 0} {
	set bitval 1
    } else {
	set bitval 0
    }
    return [format "%s.status.%3.3d.%1.1d = %d" $name $byte $bit $bitval]
}
#--------------------------------------------------
proc spss5::byteToString {b} {
    for {set i 7} {$i >=0 } {incr i -1} {
	if {($b & (1 << $i))> 0} {
	    append txt 1
	} else {
	    append txt 0
	}
    }
    return $txt
}
#----------------------------------------------------
proc spss5::bipa {name} {
    set bytelist [hval /sics/${name}/bytes]
    for {set i 0} {$i < 4} {incr i} {
	set offset [expr $i *4]
	append result [byteToString [lindex $bytelist $offset]] " " 
	incr offset
	append result [byteToString [lindex $bytelist $offset]] " "
	incr offset
	append result [byteToString [lindex $bytelist $offset]] " "
	incr offset
	append result [byteToString [lindex $bytelist $offset]] " "
	append result \n
    }
    return $result
}
#---------------------------------------------------
proc spss5::perm args {
}
#=================== Creation ======================
proc spss5::makeSPS {name hostport} {
    makesctcontroller ${name}sct std $hostport "\r\n" 60
    ${name}sct debug -1
    MakeSICSobj ${name} SPS
    hfactory /sics/${name}/bytes plain internal intar 16
    hfactory /sics/${name}/adcs plain internal intar 8

    hsetprop /sics/${name}/bytes read spss5::sendbytes
    hsetprop /sics/${name}/bytes readbytes spss5::readbytes
    ${name}sct poll /sics/${name}/bytes 20

    hsetprop /sics/${name}/adcs read spss5::sendadc
    hsetprop /sics/${name}/adcs readadc spss5::readbytes
    ${name}sct poll /sics/${name}/adcs 20

    $name makescriptfunc push "spss5::push ${name}sct" user 
    hfactory /sics/${name}/push/byte plain user int
    hfactory /sics/${name}/push/bit plain user int

    ${name} makescriptfunc adc "spss5::adc $name" spy
    hfactory /sics/${name}/adc/num plain spy int

    ${name} makescriptfunc stat2 "spss5::stat2 $name" spy
    hfactory /sics/${name}/stat2/byte plain spy int
    hfactory /sics/${name}/stat2/bit plain spy int

    ${name} makescriptfunc bipa "spss5::bipa $name" spy
    ${name} makescriptfunc perm "spss5::perm" spy
    
}
