# make the sample stick rotation using the phytron motor controller
# stickname may contain the name of the object (dom is default)
# stickconn may contain an example of a connection used in the error message when disconnected
if {![info exists stickname]} {
  set stickname dom
}
if {![info exists stickconn]} {
  set stickconn ma11-ts:3005
}
source $home/sicscommon/phytron.tcl
if {[sicsdescriptor stickrot] eq "notfound"} {
  makesctcontroller stickrot phytron unconnected 5
  phytron::make $stickname X stickrot -180 360 1
}
silent 0 stickrot reconnectinterval 0
stickrot disconnect

hsetprop /sics/stickrot/reconnect __save true
hsetprop /sics/$stickname offline_msg "disconnected - use e.g. '$stickname connect $stickconn'"
hsetprop /sics/$stickname refused_msg "controller is already connected by someone else\nERROR: use '$stickname disconnect' on the instrument where this device was last used"
