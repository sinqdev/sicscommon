#------------------------------------------------------------------------
# tecs: a script to turn on and off temperature
#
# M. Zolliker, Jun 00
#------------------------------------------------------------------------

#--------- some code to do proper initialization if necessary
set ret [catch {tecs} msg]
if {$ret != 0} {
   Publish tecs  User
}

proc tecs { { arg1 "on"} { arg2 ""} { arg3 ""} } {
  if {[string compare $arg1 "off"]==0 } {
    evfactory del temperature
    return "removed temperature"
  } elseif {[string compare $arg1 "on"]==0 } {
    evfactory new temperature tecs
    return "installed temperature via TECS"
  } else {
    temperature $arg1 $arg2 $arg3
  }
}
