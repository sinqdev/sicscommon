#-----------------------------------------------------------------------------
# This is a small driver which allows to exchange commands and replies 
# asynchronously between SICS and a device. 
#
#
# Please note that timeouts are handled by the underlying scriptcontext 
# controller and cause an error message in the reply.
#
# Mark Koennecke, August 2017
#-----------------------------------------------------------------------------

namespace eval comrep {}

#-----------------------------------------------------------------------------
proc comrep::write {name} {
    set txt [sct target]
    hupdate /sics/${name}/comFinished 0
    hupdate /sics/${name}/reply ""
    sct update $txt
    sct send $txt
    return reply
}
#----------------------------------------------------------------------------
proc comrep::reply {name} {
    set reply [sct result]
    hupdate /sics/${name}/comFinished 1
    hupdate /sics/${name}/reply $reply
    return idle
}
#-----------------------------------------------------------------------------
proc comrep::make {name controller} {
 
    MakeSicsObj $name CommandResponse

    set path /sics/${name}/command
    hfactory $path plain user text
    hsetprop $path write "comrep::write $name"
    hsetprop $path reply "comrep::reply $name"
    $controller write $path

    set path /sics/${name}/comFinished
    hfactory $path plain internal int

    set path /sics/${name}/reply
    hfactory $path plain internal text

    
}
#===============================================================================
proc comrep::transact args  {
    set comname [lindex $args 0]
    set data [join [lrange $args 1 end]]
    $comname command $data
    wait .1
    while {[sget "$comname comFinished"] != 1} {
	wait .1
    }
    return [sget "$comname reply"]
}
#------------------------------------------------------------------------------
proc comrep::send args {
    set comname [lindex $args 0]
    set data [join [lrange $args 1 end]]
    $comname command $data
    return OK
}
#------------------------------------------------------------------------------
proc comrep::available {comname} {
    return [sget "$comname comFinished"]
}
#------------------------------------------------------------------------------
proc comrep::get {comname} {
    return [sget "$comname reply"]
}
#-------------------------------------------------------------------------------
# This makes an adapter which behaves like the old rs232controller but based on
# the comrep module. Parameters:
# - name the name of the adapter
# - comname the name of a comrep module as created with the call above
#--------------------------------------------------------------------------------
proc comrep::makers232adapter {name comname} {
    MakeSicsObj $name "RS232Adapter"
 
    $name makescriptfunc send "comrep::transact $comname" user
    hfactory /sics/${name}/send/args plain user text
    
    $name makescriptfunc write "comrep::send $comname" user
    hfactory /sics/${name}/write/args plain user text

    $name makescriptfunc available "comrep::available $comname" user

    $name makescriptfunc read "comrep::get $comname" user
}

