# --------------------------------------------------------------------------
# Initialization script for a simulated TOPSI instrument
#
#
# Dr. Mark Koennecke February, 1996
#---------------------------------------------------------------------------
#                        O P T I O N S

protocol set all
# --------------- Initialize Tcl internals --------------------------------

# first all the server options are set

ServerOption ReadTimeOut  10
# timeout when checking for commands. In the main loop SICS checks for
# pending commands on each connection with the above timeout, has 
# PERFORMANCE impact!

ServerOption AcceptTimeOut  10 
# timeout when checking for connection req.
# Similar to above, but for connections

ServerOption ReadUserPasswdTimeout  500000 
# time to wiat for a user/passwd to be sent from a client. Increase this
# if there is a problem connecting to a server due to network overload\

ServerOption LogFileBaseName  /data/koenneck/src/sics/tmp/server
# the path and base name of the internal server logfile to which all
# activity will be logged.

ServerOption ServerPort 2911
# the port number the server is going to listen at. The client MUST know
# this number in order to connect. It is in client.ini

ServerOption InterruptPort 2913
# The UDP port where the server will wait for Interrupts from clients.
# Obviously, clients wishing to interrupt need to know this number.


#---------------------------------------------------------------------------
#                        U S E R S

# than the SICS users are specified
# Syntax: SicsUser name password userRightsCode
SicsUser Mugger Diethelm  1
SicsUser User   Rosy      2
SicsUser Spy    007       1


#===========================================================================
# testing the SICS epics adapter
#===========================================================================
set simMode 0
set home $env(SICSROOT)

source $home/sicscommon/mdif.tcl

mdif::make mdif localhost:8080

MakeDrive




