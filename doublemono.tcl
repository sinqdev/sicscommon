#--------------------------------------------------------------------------
# This is double crystal monochromator support using the 
# configurable virtual motor infrastructure in SICS. The double 
# crystal is characterized by 2 monochromators, each with the same 
# theta value, at a distance dist from each other. When changing lambda, the
# distance between the crystals in x has to be adjusted too. The formula
# is:
#
#  z1 = dist/tan(twotheta)
#
# and is pilfered from a formula given by P.R. Willmot at SLS
#
# Mark Koennecke, March 2012
#------------------------------------------------------------------------
namespace eval doublemono {}
set doublemono::RD 57.29577951308232087679815481410517
#-------------------------------------------------------------------------
proc doublemono::drive {val} {
    set fd [expr $val/(2.*$doublemono::dd)]
    if {$fd > 1.0} {
	error "Bad wavelength choosen"
    }
    set thetarad [expr asin($fd)]
    set distval [expr $doublemono::monodist/tan(2.*$thetarad)]
    set theta [expr $thetarad * $doublemono::RD]
    return "$doublemono::th1=$theta,$doublemono::th2=$theta,$doublemono::xdist=$distval"
#    set theta2 [expr $thetarad * $doublemono::RD]
#    set theta1 [expr (($thetarad * $doublemono::RD)+ $theta1_corr::RD)]
#    return "$doublemono::th1=$theta1,$doublemono::th2=$theta2,$doublemono::xdist=$distval"
}
#------------------------------------------------------------------------
proc doublemono::read {} {
    set l [split [$doublemono::th1] =]
    set theta [string trim [lindex $l 1]]
    set thetarad [expr $theta/$doublemono::RD]
    set lambda [expr 2.*$doublemono::dd*sin($thetarad)]
    return $lambda
}
#-------------------------------------------------------------------------
proc doublemono::check {} {
    set l [split [$doublemono::th1] =]
    set theta [string trim [lindex $l 1]]
    set thetarad [expr $theta/$doublemono::RD]
    set l [split [$doublemono::th2] =]
    set theta2 [string trim [lindex $l 1]]
    if {abs($theta - $theta2) > .01} {
   	error \
   "WARNING: thetas of double crystal monochormator out of sync: $theta versus $theta2"
    }
    set l [split [$doublemono::xdist] =]
    set truedistval [string trim [lindex $l 1]]
    set distval [expr $doublemono::monodist/tan(2.*$thetarad)]
    if {abs($truedistval - $distval) > .1} {
   	error \
   "WARNING: double crystal monochromator distance not OK, should $distval is $truedistval"
    }
}
#-------------------------------------------------------------------------
# name is the name of the variable
# th1 is the first theat motor
# th2 is the second theat motor
# xdist is the monochromator distance motor
# monodist is the distance between the two monochromators
# dd is the d-value of the monochromator reflection used
#-------------------------------------------------------------------------
proc DoubleMonoInit { name th1 th2 xdist monodist dd} {
    set doublemono::th1 $th1
    set doublemono::th2 $th2
    set doublemono::xdist $xdist
    set doublemono::monodist $monodist
    set doublemono::dd $dd
    MakeConfigurableMotor $name
    $name drivescript doublemono::drive
    $name readscript doublemono::read
    $name checkscript doublemono::check
} 
