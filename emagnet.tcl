#-----------------------------------------------------------------------
# Some Tcl to help with the new small e-magnet for SANS and 
# SANS2. There are two basic things which can be wrong with this:
#
# The box is not connected to the 138 net but to the instrument network
# The instrument network firewall blocks the necessary ports. 
#
# Mark Koennecke, November 2014
#-----------------------------------------------------------------------

set emts 129.129.138.37
set emsps 129.129.138.35

namespace eval emagnet {}

#------- First the motor
if {$simMode == 0} {
    source $home/sicscommon/phytron.tcl
    makesctcontroller emsct phytron $emts:3001 5
    phytron::make sach X emsct 0 300 0
 
}

#------------ Now the SPS

if {$simMode == 0 } {
    MakeSPSS7 ems7 200 200 $emsps:2000
}

#---------------- Now the mf motor
source $home/sicscommon/stddrive.tcl


proc emagnet::write {} {
	set alarm [sget /sics/ems7/alert]
	if {$alarm != 0} {
		sct geterror "Magnet in alarm state"
		clientlog "Magnet in alarm state, cannot do"
	} else {
		set val [sct target]
		hupdate /sics/mf/stop 0
		sput /sics/ems7/switches/setpoint $val
	}
	return idle
}
#----------------------------------------------------------
proc emagnet::read {} {
	ems7 update
	set val [sget "ems7 actual_value"]
  set sign [sget "ems7 magnet_current"]
  if {$sign == 0} {
    set sign -1
  }
	sct update [expr $val * $sign]
	return idle
}
#-----------------------------------------------------------
proc emagnet::status {name} {

  ems7 update

  set test [sget /sics/ems7/alert]
  if {$test == 1} {
  	clientlog "Magnet is in alarm status, intervene manually"
    return error
  }
  set stop [hval /sics/${name}/stop]
  if {$stop == 1} {
    return error
  }
  set target [sct target]
  set tol [hval /sics/${name}/tolerance]
  set is [hval /sics/${name}]
  if {abs($target - $is) < $tol} {
    return idle
  } else {
  	 wait .1
    [sct controller] queue /sics/${name} progress read
    return run
  }
}
#============ MF Initialisation ===========================
# I use the sctdriveobj here. For this I need a sctcontroller 
# which I do not have. All work is done through the SPS-S7. 
# I help myself with a dummy sctcontroller running the test 
# protocol
#------------------------------------------------------------ 
makesctcontroller dummy testprot

makesctdriveobj mf float user Emagnet dummy
hfactory /sics/mf/tolerance plain user float
hset /sics/mf/tolerance 0.3
hfactory /sics/mf/upperlimit plain user float
hset /sics/mf/upperlimit 75
hfactory /sics/mf/lowerlimit plain user float
hset /sics/mf/lowerlimit -75
hfactory /sics/mf/stop plain user int
hset /sics/mf/stop 0
  
hsetprop /sics/mf checklimits stddrive::stdcheck mf
hsetprop /sics/mf checkstatus emagnet::status mf
hsetprop /sics/mf write emagnet::write
hsetprop /sics/mf read emagnet::read

dummy write /sics/mf
hupdate /sics/mf -9999.99

dolater 10 dummy poll /sics/mf 60 
