#---------------------------------------------------------------
# This is the minimal Tcl code required for a motor using a 
# cooperating device client. As of now, february 2014, this is 
# protoype software. May be discarded at some point.
#
# Mark Koennecke, February 2014
#----------------------------------------------------------------

namespace eval coopmot {}

#-----------------------------------------------------------------
proc coopmot::halt {name} {
    hupdate /sics/${name}/stopmotor 1
}

#-----------------------------------------------------------------
proc coopmot::Make {name} {
    MakeSecMotor $name

    hfactory /sics/${name}/stopmotor plain internal int

    $name makescriptfunc halt "coopmot::halt $name" user

}
