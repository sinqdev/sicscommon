#------------------------------------------------------------------------------
# This is an adapter between SICS and an EPICS controlled EL737 counter.
# It works voa the EPICS scalar records. However, there are things the scalar 
# record does not support. Most notably, the scalar record has no means to 
# indicate NoBeam, Pause etc states. In order to convey this information, a 
# special monitor channel, S10,  is reused. What does not work at all is pause 
# and continue because it is not supported by the scaler record. 
#
# Yet another misuse is to use the S1 to convey the count time, scaled by 
# a factor of 1000
#
# Another misuse of scaler records fields is used to implement the threshold 
# mechanism. PR3 is used for the threshold counter, PR4 for the threshold value. 
#
# copyright: see file COPYRIGHT
#
# Mark Koennecke, January 2015
#------------------------------------------------------------------------------

namespace eval epicsel737 {}

proc epicsel737::control {name} {
	set target [sget /sics/${name}/control]
	switch $target {
		1000 {
			set mode [sget "$name mode"]
			set preset [sget "$name preset"]
			set thcter [sget "$name thresholdcounter"]
			set thval [sget "$name threshold"]
			sput /sics/${name}/PR3 $thcter
			sput /sics/${name}/PR4 $thval
			switch $mode {
				timer {
					sput /sics/${name}/TP $preset
					sput /sics/${name}/PR2 0
				}
				monitor {
					sput /sics/${name}/PR1 $preset
					sput /sics/${name}/PR2 10					
				}
			}
			sput /sics/${name}/CNT 1
		}
		1001 {
			sput /sics/${name}/CNT 0
		}
		1002 {}
		1003 {}
	}
}
#-------------------------------------------------------------------------------
proc epicsel737::cnt {name} {
	set cnt [sget /sics/${name}/CNT]
	set s10 [sget /sics/${name}/S10]
	if {$cnt == 0 } {
		sput /sics/${name}/status idle
		return
	}
	switch $s10 {
		2 {
			sput /sics/${name}/status nobeam		
		}
		3 {
			sput /sics/${name}/status pause					
		}
		default {
			sput /sics/${name}/status run						}
	}
}
#-------------------------------------------------------------------------------
proc epicsel737::time { name } {
	set tval [expr [sget /sics/${name}/S1]/1000.]
	sput /sics/${name}/time $tval
}
#-------------------------------------------------------------------------------
proc epicsel737::val {name} {
	for {set i 2} {$i < 10} {incr i} {
		lappend vl [sget /sics/${name}/S${i}]
	} 
	sput /sics/${name}/values $vl
}
#-------------------------------------------------------------------------------
proc epicsel737::make {name PVROOT} {
	MakeSecCounter $name 8

	for {set i 1} {$i <= 10} {incr i} {
		hfactory /sics/$name/S${i} plain internal int
		epicsadapter connectread /sics/$name/S${i} ${PVROOT}.S${i}
	}

	for {set i 1} {$i <= 4} {incr i} {
		hfactory /sics/$name/PR${i} plain user int
		epicsadapter connectwrite /sics/$name/PR${i} ${PVROOT}.PR${i}
	}

	hfactory /sics/$name/CNT plain user int
	epicsadapter connectwrite /sics/$name/CNT ${PVROOT}.CNT	
	epicsadapter connectread /sics/$name/CNT ${PVROOT}.CNT

	hfactory /sics/$name/TP plain user float
	epicsadapter connectwrite /sics/$name/TP ${PVROOT}.TP	

    hfactory /sics/${name}/thresholdcounter plain mugger int
    hsetprop /sics/${name}/thresholdcounter __save true
    set path /sics/${name}/threshold
    hfactory $path plain mugger int

    hscriptnotify /sics/${name}/control "epicsel737::control $name"
    hscriptnotify /sics/${name}/CNT "epicsel737::cnt $name"
    hscriptnotify /sics/${name}/S10 "epicsel737::cnt $name"
    hscriptnotify /sics/${name}/S1 "epicsel737::time $name"
    hscriptnotify /sics/${name}/S9 "epicsel737::val $name"
}
