#------------------------------------------------------------------------------
# This is a little object which triggers the imaging system of the zwickroll 
# load frame at certain intervalls. 
#
# Mark Koenneke, July 2014
#-----------------------------------------------------------------------------

namespace eval dic {}

#----------------------------------------------------------------------------
proc dic::findintervall {} {
    set zwlist [list zax1 zax2 zaxt]
    set testmot [sget "dictrigger checkmotors"]
    if {$testmot == 1} {
	foreach mot $zwlist {
	    set motstat [sget "$mot status"]
	    if {[string compare $motstat run] == 0} {
		set intervall [sget "dictrigger driveintervall"]
		return $intervall
	    }
        }
    }
    set intervall [sget "dictrigger holdintervall"]
    return $intervall
}
#----------------------------------------------------------------------------
proc getIntStatus {} {
    set statlist [list Eager Wait Counting No Paused Driving Running scan Writing Procesing]
    set stat [sget status]
    set count 0
    foreach e $statlist {
	if {[string first $e $stat] >= 0} {
	    return $count
	}
	incr count
    }
}
#-----------------------------------------------------------------------------
proc dic::sendtrigger {} {
    dicio out0 1

    set fname [sget "dictrigger datafile"]
    set f [open $fname "a"]
    set f1 [sget "zax1 forcepos"]
    set f2 [sget "zax2 forcepos"]
    set l1 [sget "zax1 pos"]
    set l2 [sget "zax2 pos"]
    set mx [sget "zaxt forcepos"]
    set m  [sget "zaxt pos"]
    set tm [sicstime]
    set dt [doubletime]
    set numor [sget sicsdatanumber]
    set stat [getIntStatus]
    set trig [sget "dictrigger trigno"]
    puts $f [format "%05d %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %d %d %18.5f %s" \
		 $trig $f1 $f2 $l1 $l2 $mx $m $numor $stat $dt $tm]
    incr trig
    dictrigger trigno $trig
    close $f
    dictrigger lasttrigger [doubletime]
}
#-----------------------------------------------------------------------------
proc dic::taskfunc {} {
    set end [sget "dictrigger stopflag"]
    if {$end == 1} {
	return 0
    }
    set intervall [findintervall]
    set last [sget "dictrigger lasttrigger"]
    if {[doubletime] > $last + $intervall} {
	sendtrigger
    }
    return 1
}
#------------------------------------------------------------------------------
proc dic::makefile {} {
    global loghome
    set t [sicstime]
    set t [string map {" " @} $t]
    set fname [format "$loghome/diclog%s.log" $t] 
    dictrigger datafile $fname
    set f [open $fname w]
    puts $f "DIC logfile started at : $t"
    set data [sget title]
    puts $f "Title : $data"
    set data [sget sample]
    puts $f "Sample : $data"
    set data [sget user]
    puts $f "User : $data"
    puts $f "#DATA"
    close $f
}
#------------------------------------------------------------------------------
proc dic::start {} {
    dictrigger stopflag 0
    dictrigger lasttrigger 0
    dictrigger trigno 0
    set status [catch {zax1} msg]
    if {$status != 0} {
	error "Cannot start, zwickroll not installed"
    }
    dic::makefile
    task run dic::taskfunc
    return OK
}
#------------------------------------------------------------------------------
proc dic::stop {} {
    dictrigger stopflag 1
    return OK
}
#-----------------------------------------------------------------------------
proc dic::connect {} {
    dictrigger checkmotors 1
    return OK
}
#-----------------------------------------------------------------------------
proc dic::disconnect {} {
    dictrigger checkmotors 0
    return OK
}
#------------------------------------------------------------------------------
proc dic::make {} {
    MakeSicsObj dictrigger DicTrigger
    hfactory /sics/dictrigger/driveintervall plain manager int
    hupdate /sics/dictrigger/driveintervall 1
    hfactory /sics/dictrigger/holdintervall plain manager int
    hupdate /sics/dictrigger/holdintervall 300
    hfactory /sics/dictrigger/checkmotors plain manager int
    hupdate /sics/dictrigger/checkmotors 0
    hfactory /sics/dictrigger/stopflag plain manager int
    hupdate /sics/dictrigger/stopflag 0
    hfactory /sics/dictrigger/datafile plain manager text
    hfactory /sics/dictrigger/lasttrigger plain manager int
    hfactory /sics/dictrigger/trigno plain manager int


    dictrigger makescriptfunc start "dic::start" manager
    dictrigger makescriptfunc stop "dic::stop" manager
 
    dictrigger makescriptfunc connect dic::connect manager
    dictrigger makescriptfunc disconnect dic::disconnect manager

    
}
#----------------------------------------------------------------------------------
if { [info exists dicinit] == 0 } {
  set dicinit 1
  dic::make   
}
