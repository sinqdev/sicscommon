#---------------------------------------------------
# A scriptcontext driven driver for the picoscope 
# frequency generator, stupid thing.
#
# Mark Koennecke, July 2017
#---------------------------------------------------

namespace eval pico {}

#----------------------------------------------------
proc pico::write {com } {
    set val [sct target]
    sct send "$com $val"
    return read
}
#-------------------------------------------------
proc pico::read {com} {
    sct send "$com"
    return picoreply
}
#-----------------------------------------------
proc pico::reply {} {
    set reply [sct result]
    set test [string tolower $reply]
    if {[string first ascerr $test] >= 0} {
	sct geterror $reply
	error $reply
    } else {
	set rep [regsub -all "\\s+" $reply " "]
	set l [split $rep]
	sct update [lindex $l 1]
    }
    return idle
}
#-------------------------------------------------
proc pico::make {hostport freq amp} {
    makesctcontroller picosct std $hostport \n 2 \n
    MakeSicsObj pico FrequencyGenerator

    set path /sics/pico/freq
    hfactory $path plain user int
    hsetprop $path write "pico::write freq"
    hsetprop $path read "pico::read freq"
    hsetprop $path picoreply "pico::reply"
    picosct poll $path 180
    picosct write $path

    set path /sics/pico/ampl
    hfactory $path plain user float
    hsetprop $path write "pico::write ampl"
    hsetprop $path read "pico::read ampl"
    hsetprop $path picoreply "pico::reply"
    picosct poll $path 180
    picosct write $path

    pico freq $freq
    pico ampl $amp
}
