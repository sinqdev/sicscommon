#---------------------------------------------------------------
# Some interface to get the scheduling data from DUO in order to 
# write it to the data file
#
# Mark Koennecke, April 2016
#---------------------------------------------------------------

namespace eval duo {}

if { [info exists __duoinit] == 0} {
    set __duoinit 1
    MakeSICSObj duo duo
    hfactory /sics/duo/beamline plain internal text
    hfactory /sics/duo/proposal plain internal text
    hfactory /sics/duo/title plain internal text
    hfactory /sics/duo/firstname plain internal text
    hfactory /sics/duo/lastname plain internal text
    hfactory /sics/duo/email plain internal text
    hfactory /sics/duo/start plain internal text
    hfactory /sics/duo/end plain internal text
    duo makescriptfunc parsejson duo::parsejson mugger  
    hfactory /sics/duo/parsejson/jsontxt plain mugger text

    sicsdatafactory new _duotransfer
    makesctcontroller duosct sinqhttpopt duo.psi.ch _duotransfer
#    makesctcontroller duosct sinqhttpopt localhost:8080 _duotransfer
    set inst [string tolower [string trim [sget instrument]]]
    if {[string compare $inst rita2] == 0 } {
	set inst rita-ii
    }
    if {[string first SANS-II $inst] >= 0} {
	set inst sans-ii
    }
    hsetprop /sics/duo/beamline read "duo::duossend $inst"
    hsetprop /sics/duo/beamline reply "duo::duoreply"

    duosct poll /sics/duo/beamline 3600
} 
#---------------------------------------------------------------
proc duo::parsejson {jsontxt} {
#     clientlog $jsontxt
     set l [split $jsontxt ,]
     foreach item $l {
	set l2 [split $item :]
	set key [string trim [lindex $l2 0] \"]
	set value [string trim [lindex $l2 1] \"]
#	clientlog "$key = $value"
	hupdate /sics/duo/${key} $value
    }
}
#--------------------------------------------------------------
proc duo::duossend {inst} {
    sct send "/public/rest.php/cal/sinq/${inst}/?SECRET=Vvo9tYGBvR0AAHgQHZcAAAAH"
#    clientlog "/public/rest.php/cal/sinq/${inst}/?SECRET=Vvo9tYGBvR0AAHgQHZcAAAAH"
    return reply
}
#------------------------------------------------------------
proc duo::duoreply {} {
    set reply [sct result]
    if {[string first ASCERR $reply] >= 0 || [string first ERROR $reply] >= 0} {
	sct geterror $reply
	error $reply
    } 
    hdelprop [sct] geterror
    if {[string length $reply] > 5} {
	duo::parsejson [string trim $reply \{\}]
    }
    return idle
}
#------------------------------------------------------------

proc writeDuo {} {
    set first [hval /sics/duo/firstname]
    set last [hval /sics/duo/lastname]
    set name "$first $last"
    nxscript puttext duouser $name
    nxscript puts duoemail /sics/duo/email
    nxscript puts duoid /sics/duo/proposal
    nxscript puts duotitle /sics/duo/title
}
#-----------------------------------------------------------
proc formatDuo {} {
    set prop [hval /sics/duo/proposal]
    append result "proposal = $prop\n"
    set first [hval /sics/duo/firstname]
    set last [hval /sics/duo/lastname]
    append result "proposal_user = $first $last\n"
    set title [hval /sics/duo/title]
    append result "proposal_title = $title\n"
    set email [hval /sics/duo/email]
    append result "proposal_email = $email\n"
}
