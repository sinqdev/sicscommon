proc AsciiPlot_findScale {ydatalist scale baseline} {
upvar $scale sc
upvar $baseline bl
    set min +99999999.99
    set max -99999999.99
    foreach yval $ydatalist {
	if {$yval > $max} {
	    set max $yval
	}
	if {$yval < $min} {
	    set min $yval
	}
    }
    set sc [expr 61./($max-$min)]
    set bl [expr int(-$min*$sc+1.)]

}

proc AsciiPlot_clearLine {line} {
upvar $line Zeile
    for {set i 0} {$i < 64} {incr i} {
	set Zeile($i) " "
    }
    set Zeile(64) "\n"
}

proc AsciiPlot_printLine {xtxt line} {
upvar $line Zeile
    set txtline ""
    set txtline "$txtline$xtxt"
    for {set i 0} {$i <= 64} {incr i} {
	set txtline "$txtline$Zeile($i)"
    }
    ClientPut $txtline
}

proc AsciiPlot_list {xdata ydata} {
    AsciiPlot_findScale $ydata scale baseValue
    set xty 0
    set avgy 0
    foreach xval $xdata yval $ydata {
	set xty [expr $xty+$xval*$yval]
	set avgy [expr $avgy+$yval]
	AsciiPlot_clearLine line
	set line(0) "!"
	set height [expr int($yval*$scale+$baseValue)]
	if {$height >= 1} {
	    if {$height < 69} {
		set line($height) "*"
	    } else {
		set line(68) "*"
	    }
	}
	AsciiPlot_printLine [format %+#1.3e $xval] line
    }
    ClientPut "\ncenter of gravity = [expr 1.*$xty/$avgy]\n"
}

proc AsciiPlot_xydata2list {xydatalist xdata ydata} {
upvar $xdata xd
upvar $ydata yd
set xd {}
set yd {}
set xydl [$xydatalist list]
   foreach {x y} $xydl {
       lappend xd $x
       lappend yd $y
   }
}

proc AsciiPlot {data} {
AsciiPlot_xydata2list $data xdata ydata
AsciiPlot_list $xdata $ydata
}

Publish AsciiPlot Spy
#alias asciiplot AsciiPlot
