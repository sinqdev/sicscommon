#-------------------------------------------------------------
# Some code to connect to the SINQ status as read from EPICS
# via lns00. To replace the dead UDP broadcast service used 
# until 2013.
#
# Mark Koennecke, June 2014
#-------------------------------------------------------------

namespace eval sinq {}

proc sinq::readflux {} {
    sct send "/cgi-bin/hipaflux.cgi"
    return fluxreply
}
#--------------------------------------------------------
proc sinq::hmhttptest {data} {
    if {[string first ASCERR $data] >= 0} {
	error $data
    }
    if {[string first ERROR $data] >= 0} {
	error $data
    }
    return $data
}
#-------------------------------------------------------------
proc sinq::fluxreply {} {
    set reply [sct result]
    set status [catch {hmhttptest $reply} data]
    if {$status == 0} {
	hdelprop [sct] geterror
	sct update $data
	set lines [split $data \n]
	set ringtext [lindex $lines 2]
	set ringl [split $ringtext :]
	hupdate /sics/sinqimpl/ring [string trim [lindex $ringl 1]]
	set beamtext [lindex $lines 5]
	set beaml [split $beamtext :]
	hupdate /sics/sinqimpl/beam [string trim [lindex $beaml 1]]
    } else {
	[sct] geterror $data
    }
    return idle
}
#-------------------------------------------------------------
proc sinq::readstatus {} {
    sct send "/cgi-bin/hipatext.cgi"
    return statusreply
}
#-------------------------------------------------------------
proc sinq::statusreply {} {
    set reply [sct result]
    set status [catch {hmhttptest $reply} data]
    if {$status == 0} {
	hdelprop [sct] geterror
	sct update $data
    } else {
	[sct] geterror $data
    }
    return idle
}
#------------------------------------------------------------
proc sinq args {
    if {[llength $args] < 1} {
	sinqsct processnode /sics/sinqimpl/flux
	sinqsct processnode /sics/sinqimpl/status
	append result [hval /sics/sinqimpl/flux]
	append result "---------- Messages -------------\n"
	append result [hval /sics/sinqimpl/status]
	return $result
    }

    
    sinqsct processnode /sics/sinqimpl/flux
    switch [lindex $args 0] {
	beam {
	    set val [sget /sics/sinqimpl/beam]
	    return "sinq.beam = $val"
	}
	ring {
	    set val [sget /sics/sinqimpl/ring]
	    return "sinq.ring = $val"
	}
	default {
	    error "sinq understands only beam and ring subcommands" 
	}
    }
}
#-------------------------------------------------------------
proc sinq::make {} {
    sicsdatafactory new _sinqtransfer
    makesctcontroller sinqsct sinqhttpopt lns00 _sinqtransfer
    MakeSicsObj sinqimpl SINQ
    hfactory /sics/sinqimpl/beam plain internal text
    hfactory /sics/sinqimpl/ring plain internal text
    hfactory /sics/sinqimpl/flux plain internal text
    hfactory /sics/sinqimpl/status plain internal text

    hsetprop /sics/sinqimpl/flux read sinq::readflux
    hsetprop /sics/sinqimpl/flux fluxreply sinq::fluxreply
    sinqsct poll /sics/sinqimpl/flux 300 

    hsetprop /sics/sinqimpl/status read sinq::readstatus
    hsetprop /sics/sinqimpl/status statusreply sinq::statusreply
    sinqsct poll /sics/sinqimpl/status 300 

    Publish sinq Spy
}
