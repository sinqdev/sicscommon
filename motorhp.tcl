#---------------------------------------------------------------------------
# These scripts save and load motor positions for EL734 motors connected
# directly to SICS through the terminal server. For all others, use
# David Madens el734_motor program
#
# Mark Koennecke, April 2004
#-------------------------------------------------------------------------

if { ![info exists motorhpscript] } {
    set motorhpscript 1
    Publish motorinternsave Mugger
    Publish motorsave     Mugger
    Publish motorload     Mugger
    Publish loadmotordir  Mugger
    Publish savemotorarray Mugger
}

#----------------------------------------------------------------------
# save motor parameters from controller, number to file described by
# file descriptor fd
#----------------------------------------------------------------------
proc motorinternsave {controller number fd} {
    lappend parlist mn ec ep a fd fm d e f g h j k l m q t v w z mem
    puts $fd [format "%s send ec %d 0 0" $controller $number]
    foreach e $parlist {
	set data [$controller send $e $number]
	puts $fd [format "%s send %s %d %s" $controller $e $number $data]
    }
}
#----------------------------------------------------------------------
# save a motor parameter set to a directory. The filename is automatically
# created in order to help motorload
#---------------------------------------------------------------------
proc motorsave {controller number dirname} {
    set filename [format "%s/%s%2.2d.par" $dirname $controller $number]
    set f [open $filename w]
    motorinternsave $controller $number $f
    close $f
}
#----------------------------------------------------------------------------
# Loading motor parameters. Because some of the commands change the position
# of the motor, the position is saved first and redefined after processing
# the data. It is assumed that the filename is in the format as made
# by motorsave.
#---------------------------------------------------------------------------
proc motorload {filename} {
    set fil [file tail $filename]
    set ind [string last . $fil]
    set number [string range $fil [expr $ind - 2] [expr $ind - 1]]
    set controller [string range $fil 0 [expr $ind - 3]]
    set pos [$controller send u $number]
    fileeval $filename
    $controller send uu $number $pos
}
#--------------------------------------------------------------------------
# load a motor directory
#------------------------------------------------------------------------
proc loadmotordir {dirname} {
    set l [glob $dirname/*.par]
    foreach e $l {       
	set ret [catch {motorload $e} msg]
	if { $ret != 0} {
	    clientput "ERROR: failed to load $e with $msg"
	}
    }
}
#-----------------------------------------------------------------------
# save a whole array of motors. The array must have the following form:
# An entry: controllerlist conatins a list of all controllers
# There exists an entry with the controller name in the array which contains
# a list of motor number
#------------------------------------------------------------------------
proc savemotorarray {motar dir} {
    upvar $motar motorarray
    set controllerList $motorarray(controllerlist)
    foreach controller $controllerList {
	set motlist $motorarray($controller)
	foreach mot $motlist {
	    motorsave $controller $mot $dir
	}
    }
}
