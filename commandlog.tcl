#---------------------------------------------------------------------------
# This is a new implementation of commandlog based on the mongodb querying 
# function.
#
# copyright: see file COPYRIGHT
#
# Mark Koennecke, March 2016
#---------------------------------------------------------------------------

if { [info exists __cominit] == 0 } {
  set __cominit 1
  Publish commandlog Spy
}
 
#---------------------------------------------------------------------------
proc commandlog args {
    if {[llength $args] < 1 } {
	error "need argument to commandlog"
    }
    set key [lindex $args 0]
    switch $key {
	tail {
	    set t 1
	    if {[llength $args] > 1} {
		set t [lindex $args 1]
	    }
	    showlog -s com -l $t
	}
	default {
	    error "$key no known subcommand to commandlog"
	}
    }
}