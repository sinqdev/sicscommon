#--------------------------------------------
# The old batchrun, batchroot pair
# Mark Koennecke, since 1996
#--------------------------------------------

if { [info exists batchinit] == 0 } {
    set batchinit 1
    Publish batchroot Spy
    Publish batchrun User 
}

proc SplitReply { text } {
     set l [split $text =]
     return [lindex $l 1]
}
#---------------------
proc batchrun file {
   exe [string trim [SplitReply [batchroot]]/$file]
}
#---------------------
proc batchroot args {
  if {[llength $args] >= 1} {
    exe batchpath [lindex $args 0]
    return OK
  } else {
    set bp [SplitReply [exe batchpath]]
    return "batchroot = $bp"
  }
}
