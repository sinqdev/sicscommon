#-----------------------------------------------------
# This is a simulation driver for the second 
# generation histogram memory. It provides 
# for a  fill value which is used to initialize
# data. 
#
# copyright: see file COPYRIGHT
#
# Mark Koennecke, January 2010
#----------------------------------------------------- 
namespace eval simhm {}
#-----------------------------------------------------
proc simhm::getcontrol {name} {
    return -9999.99
}
#----------------------------------------------------
proc simhm::setcontrol {name val} {
    switch $val {
	1000 {
	    hset /sics/${name}/internalstatus run
	    set pp [hval /sics/${name}/preset]
	    hset /sics/${name}/finishtime [expr $pp + [clock seconds]]
	    return idle
	}
	1001 {
	    hset /sics/${name}/internalstatus error
	    return idle
	}
	1002 {
	    hset /sics/${name}/internalstatus pause
	    return idle
	}
	1003 {
	    hset /sics/${name}/internalstatus run
	    return idle
        }
        1005 {
	    return idle
	}
	default {
	    clientput "ERROR: bad start target $target given to control"
	    return idle
	}
    }
}
#----------------------------------------------------
proc simhm::getstatus {name} {
    set status [string trim [hval /sics/${name}/internalstatus]]
    if {[string first run $status] >= 0 || [string first error $status] >= 0} {
	set fin [string trim [hval /sics/${name}/finishtime]]
	if {[clock seconds] > $fin} {
	    hset /sics/${name}/internalstatus idle
	    set val [string trim [hval /sics/${name}/initval]]
	    $name set $val
	    set second [string trim [hval /sics/${name}/secondbank]]
	    if {[string compare $second NULL] != 0} {
		harray /sics/${name}/${second} init $val
            }
        }
    }
    return $status
}
#-----------------------------------------------------
proc simhm::MakeSimHM {name rank {tof  NULL} } {
    MakeSecHM $name $rank $tof
    hfactory /sics/${name}/initval plain user int
    hset /sics/${name}/initval 0

    hfactory /sics/${name}/finishtime plain user int
    hfactory /sics/${name}/internalstatus plain user text
    hupdate /sics/${name}/internalstatus idle

    hdel /sics/${name}/control
    hfactory /sics/${name}/control script \
	"simhm::getcontrol $name" "simhm::setcontrol $name" float
    hsetprop /sics/${name}/control priv user
    
    hdel /sics/${name}/status
    hfactory /sics/${name}/status script \
	"simhm::getstatus $name" hdbReadOnly text
    hsetprop /sics/${name}/control priv user
    hupdate /sics/${name}/status idle

    hfactory /sics/${name}/secondbank plain user text
    hupdate  /sics/${name}/secondbank NULL
}
#------------------------------------------------------
proc simhm::makeSecond {name bankname length} {
    hfactory /sics/${name}/${bankname} plain user intvarar $length
    hupdate /sics/${name}/secondbank $bankname
}
