#---------------------------------------------------------------------
# This is a second generation driver for another weird piece of 
# SINQ hardware, the Emmenegger electronics. That thing duplicates 
# the chopper signal in order to account for the second slit in the 
# chopper disk. It also allows to adjust an obscure time delay 
# and an acceptance window. 
#
# Mark Koennecke, July 2017
#---------------------------------------------------------------------

namespace eval emmi {}

#--------------------------------------------------------------------
proc emmi::write {com} {
    set val [sct target]
    sct send [format "%s %f" $com [expr $val*10]]
    return writereply
}
#--------------------------------------------------------------------
proc emmi::wreply {com} {
    set reply [sct result]
    if {[string match -nocase "ASCERR: *" $reply]} {
	sct geterror $reply
	error $reply
    }
    return [emmi::read $com]
}
#---------------------------------------------------------------------
proc emmi::read {com} {
    sct send $com
    return reply
}
#--------------------------------------------------------------------
proc emmi::reply {} {
    set reply [sct result]
    if {[string match -nocase "ASCERR: *" $reply]} {
	sct geterror $reply
	error $reply
    }
    hdelprop [sct] geterror
    sct update [expr $reply/10.]
    return idle
}
#-----------------------------------------------------------------------
# The script chaining is such that after a write a readback is initiated
# and the script finishes only when the readback has finished
#-----------------------------------------------------------------------
proc emmi::make {hostport} {
    makesctcontroller emmisct std $hostport 0xd  1 0xd

    MakeSicsObj emmi EMMENEGGER

    set path /sics/emmi/td
    hfactory $path plain Manager float
    hsetprop $path write "emmi::write D"
    hsetprop $path writereply "emmi::wreply D"
    hsetprop $path read "emmi::read D"
    hsetprop $path reply "emmi::reply"
    emmisct poll $path 90
    emmisct write $path


    set path /sics/emmi/aw
    hfactory $path plain Manager float
    hsetprop $path write "emmi::write W"
    hsetprop $path writereply "emmi::wreply W"
    hsetprop $path read "emmi::read W"
    hsetprop $path reply "emmi::reply"
    emmisct poll $path 90
    emmisct write $path

}   

