#--------------------------------------------------------------
# This is the initialisation code for the ANDOR iKon-M
# camera and the CDDWWW WWW-server. It got separated into 
# a separate file in order to support moving that camera around.
#
# Mark Koennecke, November 2010
#--------------------------------------------------------------

#source $scripthome/ccdwww.tcl

#---------------------------------------------------------------
set ccdwww::initnodes [list daqmode camerano accucycle accucounts \
  			   triggermode temperature imagepar shutterlevel \
  			   shuttermode openingtime closingtime flip rotate \
			    hspeed vspeed vamp writetiff]
#--------------------------------------------------------------
proc writecooler {} {
    set target [sct target]
    set status [ccdwww::httpsend "/ccd/cooling?status=$target"]
    hmsct queue /sics/hm/cooler read read
    hmsct queue /sics/hm/sensor_temperature read read
    return $status
}
#--------------------------------------------------------------
proc readcooler {} {
    sct send "/ccd/iscooling"
    return coolerreply
}
#---------------------------------------------------------------
proc coolerreply {} {
    set reply [sct result]
    set status [catch {ccdwww::httptest $reply} data]
    if {$status != 0} {
      sct geterror $data
      clientput $data
    } else {
        catch {hdelprop [sct] geterror}
	if {$data == 0} {
	    sct update off
	} else {
	    sct update on
        }
    }
    return idle
}
#---------------------------------------------------------
proc readtemp {} {
    ccdwww::httpsend "/ccd/temperature"
    return tempreply
}
#--------------------------------------------------------
proc tempreply {} {
    set reply [sct result]
    set status [catch {ccdwww::httptest $reply} data]
    if {$status != 0} {
      sct geterror $data
      clientput $data
    } else {
        catch {hdelprop [sct] geterror}
	sct update $data
    }
    return idle
}
#-------------------------------------------------------------
proc MakeAndorHM {name host } { 
  ccdwww::MakeCCDWWW $name $host "ccdwww::initscript $name"
  hfactory /sics/$name/daqmode plain mugger text
  hset /sics/$name/daqmode single
  hfactory /sics/$name/camerano plain mugger int
  hset /sics/$name/camerano 0
  hfactory /sics/$name/accucycle plain mugger int
  hset /sics/$name/accucycle 20
  hfactory /sics/$name/accucounts plain mugger int
  hset /sics/$name/accucounts 5
  hfactory /sics/$name/triggermode plain mugger int
  hset /sics/$name/triggermode 7
  hfactory /sics/$name/temperature plain mugger int
  hset /sics/$name/temperature -70
  hfactory /sics/$name/imagepar plain mugger intar 6
  hset /sics/$name/imagepar 1 1 1 1024 1 1024
  hfactory /sics/$name/shutterlevel plain mugger int
  hset /sics/$name/shutterlevel 0
  hfactory /sics/$name/shuttermode plain mugger int
  hset /sics/$name/shuttermode 0
  hfactory /sics/$name/openingtime plain mugger int
  hset /sics/$name/openingtime 20
  hfactory /sics/$name/closingtime plain mugger int
  hset /sics/$name/closingtime 20
  hfactory /sics/$name/flip plain mugger intar 2
  hset /sics/$name/flip 1 0
  hfactory /sics/$name/rotate plain mugger int
  hset /sics/$name/rotate 0
  hfactory /sics/$name/hspeed plain mugger int
  hset /sics/$name/hspeed 2
  hfactory /sics/$name/vspeed plain mugger int
  hset /sics/$name/vspeed 0
  hfactory /sics/$name/vamp plain mugger int
  hset /sics/$name/vamp 1
  hfactory /sics/$name/cooler plain mugger text
  hset /sics/$name/cooler off
  hsetprop /sics/$name/cooler write writecooler
  hsetprop /sics/$name/cooler httpreply  ccdwww::httpreply
  hsetprop /sics/$name/cooler read readcooler
  hsetprop /sics/$name/cooler coolerreply  coolerreply
  ${name}sct write /sics/$name/cooler
  ${name}sct poll /sics/$name/cooler 30
  hfactory /sics/$name/sensor_temperature plain mugger float
  hsetprop /sics/$name/sensor_temperature read readtemp
  hsetprop /sics/$name/sensor_temperature tempreply tempreply
  ${name}sct poll /sics/$name/sensor_temperature 30
  $name dim 1024 1024

  hfactory /sics/${name}/writetiff plain mugger int
  $name writetiff 0

  $name init
}
