#-------------------------------------------------------------------------------------
# This is a second generation replacement for the MDIF interface. Basically, this 
# is a device which sucks. It only supports setting the delay time but does not 
# allow to read the value back. It is also very sensitive to the right hardware 
# being deployed. Especially the fibre optic RS-232 adapter of which some work 
# and some do not. The only way to test this is by setting  a value and checking 
# at the device if it has arrived or not.   
#
# Mark Koennecke, July 2017
#---------------------------------------------------------------------------------------

namespace eval mdif {}

#----------------------------------------------------------------------------------------
proc mdif::write {} {
    set val [sct target]
    sct send "DT $val"
    sct dtval $val
    return reply
}
#----------------------------------------------------------------------------------------
proc mdif::reply {} {
    set reply [sct result]
    if {[string match -nocase "ASCERR: *" $reply]} {
	if {![string match -nocase "*no response*" $reply]} {
	    broadcast "ERROR: MDIF reports $reply. This is critical!!"
        }
    } 
    sct update [sct dtval]
    return idle
}
#----------------------------------------------------------------------------------------
proc mdif::make {name hostport} {
    set con ${name}sct
    makesctcontroller $con std $hostport 0xd  1 0xd
    $con send "RMT 1"
    $con send "ECHO 0"
    
    MakeSicsObj $name MDIF
    set path /sics/${name}/dt
    hfactory $path plain user int
    hsetprop $path write mdif::write
    hsetprop $path reply mdif::reply
    $con write $path
}

