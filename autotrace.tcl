#-------------------------------------------------------------------------
# This installs an autotrace facility into SICS. Autotrace calculates a 
# daily filename and saves a trace to it. If the file already exists
# it will be appended too.
#
# This runs through sicscron
#
# Mark Koennecke, May 2014
#-------------------------------------------------------------------------

namespace eval autotrace {}

#--------------------------------------------------------------------------
# I call unknown explicitly in the code belo in order to force the execution
# as a SICS command. Unfortnatley trace is also a Tcl command with a very 
# different meaning and there is a name collision.
#--------------------------------------------------------------------------
proc autotrace::autorun {} {
    global loghome

    set datetext [clock format [clock seconds ] -format %Y-%m-%d]
    set filename ${loghome}/autotrace-${datetext}.log
    set tracestate [sget "trace log"]
    if {[string first $filename $tracestate] < 0} {
	unknown trace append $filename
    }
}

#--------------------------------------------------------------------------
proc installAutoTrace {} {
    autotrace::autorun
    sicscron 600 autotrace::autorun
}

#------------------------------------------------------------------------------
# The cronjob to run in order to kill older autotrace is:
#
# 0 1 * * * /bin/find /home/inst/log/autotrace* -mtime +7 -exec /bin/rm {} \;
#------------------------------------------------------------------------------