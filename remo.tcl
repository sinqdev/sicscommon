#----------------------------------------------------------------------
# This is a number of Tcl helper routines for remote object processing.
# Most notably it implements proxying remote objects. 
#
# COPYRIGHT: see file COPYRIGHT
#
# Mark Koennecke, March 2015
#-----------------------------------------------------------------------

namespace eval remo {}

#-----------------------------------------------------------------------
# List entries are of the form: obj subnode = value
# We want to cut of the subnode part
#-----------------------------------------------------------------------
proc remo::prepareRemoteList {remote remo} {
    set rawtext [$remo exe $remote list]
    set rawlist [split $rawtext \n]
    foreach entry $rawlist {
	set el [split $entry =]
	set nl [split [lindex $el 0]]
	set node [lindex $nl 1]
	if {[string length $node] > 0 && [string compare $node OK] != 0} {
	    lappend nodelist $node	 
	}
    }
    return $nodelist
}
#----------------------------------------------------------------------
proc remo::nodeExists {local node} {
    set test [catch {sget /sics/${local}/${node}} msg]
    if {$test == 0} {
	return 1
    } else {
	return 0
    }
}
#----------------------------------------------------------------------
proc remo::createNode {local node remote remo } {
    set description [$remo exe hinfo /sics/${remote}/${node}]
    set plist [split $description ,]
    set type [lindex $plist 0]
    set length [lindex $plist 2]
    switch $type {
	func {
	    eval $local makescriptfunc $node \"$remo exe $remote $node \" user 
	}
	default {
	    eval hfactory /sics/${local}/${node} plain user $type $length
	}
    }
}
#----------------------------------------------------------------------
proc remo::connectNode {local node remote remo exwritelist} {
    $remo connectread /sics/${local}/${node} /sics/${remote}/${node}
    if { [lsearch $exwritelist $node] < 0} {
	$remo connectwrite /sics/${local}/${node} /sics/${remote}/${node}
    }
}
#----------------------------------------------------------------------
# local is the local object
# remote is the remote object
# remo is the remote object adapter
#----------------------------------------------------------------------
proc remo::copynodes {local remote remo exwritelist} {
    set remotelist [prepareRemoteList $remote $remo]
    foreach node $remotelist {
	if {![nodeExists $local $node]} {
	    createNode $local $node $remote $remo
        } 
	connectNode $local $node $remote $remo $exwritelist
    }
}
#----------------------------------------------------------------------
proc remoteobjexe args {
    set com [join $args]
    return [eval $com]
}
#--------------------------------------------------------------------
proc remo::term {statusnode} {
    set txt [sget $statusnode]
    if {[string compare $txt run] != 0} {
	return idle
    } else {
	return busy;
    }
}
#---------------------------------------------------------------------
proc remo::proxymotor {local remote remo} {
    set status [catch {sget $local} msg]
    if {$status == 0} {
	error "$local already exists!"
    }
    MakeSecMotor $local
    set exwritelist [list hardposition targetposition status error]
    copynodes $local $remote $remo $exwritelist
    $remo connectwrite /sics/${local} /sics/${remote}
}
