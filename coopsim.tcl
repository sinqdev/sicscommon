#!/usr/bin/tclsh
#----------------------------------------------------------------------------
# This is a prototype cooperating device driver for SICS in simulation mode 
# This code is experimental and more a proof of concept.
#
# Mark Koennecke, February 2014
#-----------------------------------------------------------------------------

set devicewait idle
set notifywait idle

set state connecting
set sics "localhost:2911"
set notifySock ""
set commandSock ""
set rbrunning 0
set rcrunning 0

#==================== device specific stuff =================================
set motlist [list rc rb]

proc openDevice {} {
    puts stdout "Opening simulation device"
}
#-----------------------------------------------------------------------------
proc startDevice {name target} {
    global rctarget rbtarget rbfinish rcfinish rbrunning rcrunning
    puts stdout "Run $name to $target"

    switch $name {
	rb {
	    set rbtarget $target
	    set rbfinish [expr [clock seconds] + 7]
	    set rbrunning 1
	}
	rc {
	    set rctarget $target
	    set rcfinish [expr [clock seconds] + 7]
	    set rcrunning 1
	}
    }
}
#-----------------------------------------------------------------------------
proc stopDevice {name} {
    global rcrunning rbrunning
    global commandSock
    puts stdout "Stopping $name"
    sicscom $commandSock "hupdate /sics/${name}/status idle"
    sicscom $commandSock "hupdate /sics/${name}/stopmotor 0"
    switch {$name} {
	rb {
	    set rbrunning 0
	}
	rc {
	    set rcrunning 0
	}
    }
}
#----------------------------------------------------------------------------
proc checkDevices {} {
    global rbtarget rctarget rcrunning rbrunning rbfinish rcfinish commandSock state

    if {$rbrunning == 1} {
	if {[clock seconds] > $rbfinish} {
	    sicscom $commandSock "hupdate /sics/rb/hardposition $rbtarget"
	    sicscom $commandSock "hupdate /sics/rb/stopmotor 0"
	    sicscom $commandSock "hupdate /sics/rb/status idle"
	    set rbrunning 0
        }
    }
    if {$rcrunning == 1} {
	if {[clock seconds] > $rcfinish} {
	    sicscom $commandSock "hupdate /sics/rc/hardposition $rctarget"
	    sicscom $commandSock "hupdate /sics/rc/stopmotor 0"
	    sicscom $commandSock "hupdate /sics/rc/status idle"
	    set rcrunning 0
        }
    }
    if {$rbrunning == 0 && $rcrunning == 0} {
	set state waiting
    }
}
#================================ main =======================================
proc sicscom {sock command} {
    puts $sock $command
    flush $sock
    puts stdout "TO SICS>$command"
    set reply [gets $sock]
    puts stdout "FROM SICS>$reply"
}
#----------------------------------------------------------------------------
proc readNotify {} {
    global notifywait notifySock state
    set notifywait data
    if {[eof $notifySock] } {
	puts stdout "Disconnect identified"
	after 2000
	set state connecting
    }
}
#-----------------------------------------------------------------------------
proc connectCoop {} {
    global state sics notifySock commandSock motlist

    set adl [split $sics :]
    set host [lindex $adl 0]
    set port [lindex $adl 1]
    set available 0
    while {$available == 0} {
	set status [catch {socket $host $port} notifySock]
	if {$status == 0} {
	    set available 1
	} else {
	    after 20000
	}
    }
    gets $notifySock
    sicscom $notifySock "Achterbahn Kiel"
    foreach mot $motlist {
	sicscom $notifySock "hnotify /sics/${mot}/targetposition 78"
	sicscom $notifySock "hnotify /sics/${mot}/stopmotor 78"
    }
    fconfigure $notifySock -blocking false
    fileevent $notifySock readable readNotify 
    set commandSock [socket $host $port]
    gets $commandSock
    sicscom $commandSock "Achterbahn Kiel"
    set state waiting
    openDevice
}
#---------------------------------------------------------------------------
proc doNotify {} {
    global notifywait notifySock motlist state
    
    while {[gets $notifySock line] >= 0} {
	if {[eof $notifySock] } {
	    puts stdout "Disconnect identified"
	    set state connecting
	    after 2000
	    return
	}
	puts stdout "Received notification:$line"
	set l [split $line /]
	set mot [lindex $l 2]
	set l [split $line =]
	set target [lindex $l 1]
	if {[string first stopmotor $line] > 0} {
	    if {$target == 1} {
		stopDevice $mot
	    }
	} elseif {[string first targetposition $line] > 0} {
	    startDevice $mot $target
	    set state monitoring
	}
    }
    set notifywait idle
}
#===================== main loop =============================================
while {1} {
    switch $state {
	connecting {
	    connectCoop
	}
	waiting  {
	    vwait notifywait
	    doNotify
	}
	monitoring {
	    after 100 {set devicewait data}
	    vwait devicewait 
	    if {[string compare $notifywait idle] != 0} {
		doNotify
	    }
	    set devicewait idle
	    checkDevices
	}
    } 
}
