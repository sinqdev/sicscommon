#---------------------------------------------------------------------------
# Driver code for the Zwick and Roll 3-Axis materials testing machine.
#
# Mark Koennecke, June - July 2013
# Mark Koennecke, October 2013
#---------------------------------------------------------------------------

namespace eval zwickroll {}

set zwickroll::names [list zax1 zax2 zaxt]
set zwickroll::state idle
set zwickroll::modes [list force pos tension]
set zwickroll::comlist [list]
set zwickroll::comptr 0
set zwickroll::replydata ""
set zwickroll::waitlist [list]
set zwickroll::waitcount 0
set zwickroll::breakcodes [list 40608 30743 30744]
set zwickroll::limcodes [list 30734 30741 30742]
set zwickroll::log1 [list off force1 2]
set zwickroll::log2 [list off force2 2]
set zwickroll::log3 [list off force3 2]
set zwickroll::sicsstate -1
 

proc zwickroll::check {name} {
    return OK
}
#------------------------------------------------------------------------
proc zwickroll::calculateDirections {} {
    global zwickroll::names zwickroll::comlist zwickroll::comptr
    global zwickroll::replydata
    
    foreach ax $zwickroll::names {
	set target [hval /sics/${ax}/targetposition]
	if {$target >= 0} {
	    lappend dirlist 0
        } else {
	    lappend dirlist 1
	}
    }
    set com [format "TI_GETB \"M_SetTypeOfTest(%s)\"" [join $dirlist ,]]
#    set zwickroll::comlist [list TI_STOP $com TI_START]
    set zwickroll::comlist [list]
    set zwickroll::comptr 0
    set zwickroll::replydata ""
    set zwickroll::waitlist [list]
    set zwickroll::waitcount 0
}
#--------------------------------------------------------------------------
proc zwickroll::settimeout {axis speed drivemode targetmode target } {
    if {[string compare $drivemode force] == 0 && 
	[string compare $targetmode force] == 0} {
	set pos [sget $axis]
	set drivetime [expr abs((double($pos) - double($target))/double($speed))]
	set drivetimeout [expr [clock seconds] + $drivetime + 120]
	hupdate /sics/${axis}/drivetimeout $drivetimeout
	set now [clock seconds]
	clientlog "$axis: now, drivetimeout, speed = $now, $drivetimeout, $speed"
    } else {
	hupdate /sics/${axis}/drivetimeout -1
    }
}
#--------------------------------------------------------------------------
proc zwickroll::checktimeout {} {
    global zwickroll::names zwickroll::waitcount

    foreach mot $zwickroll::names {
	set active [sget "/sics/${mot}/active"]
	set drivetimeout [sget "/sics/${mot}/drivetimeout"]
	if {$active == 1 && $drivetimeout > 0 } {
	    if {[clock seconds] > $drivetimeout} {
		incr zwickroll::waitcount -1
		hupdate /sics/${mot}/active 0
            } 
        }
    }
}
#---------------------------------------------------------------------------
proc zwickroll::addAxis1Par {ax} {
    global zwickroll::comlist

    set speed [hval /sics/${ax}/speed]
    set dmode [hval /sics/${ax}/drivemode]
    set tmode [hval /sics/${ax}/targetmode]
    set hmode [hval /sics/${ax}/holdmode]
    set target [hval /sics/${ax}/targetposition]
    zwickroll::settimeout $ax $speed $dmode $tmode $target

    switch $tmode {
	force {
	    lappend comlist "TI_DO \"C\[18461\] = 40408\""
	}
	pos {
	    lappend comlist "TI_DO \"C\[18461\] = 40409\""
        }
	tension {
	    lappend comlist "TI_DO \"C\[18461\] = 40407\""
        }
	default {
	    error "Invalid targetmode $dmode"
        }
    }
    lappend comlist "TI_DO \"P\[18461\] = $target\""

    lappend comlist "TI_DO \"P\[18462\] = 0.0001\""
    switch $dmode {
	force {
	    lappend comlist "TI_DO \"C\[18462\] = 40900\""
	}
	pos {
	    lappend comlist "TI_DO \"C\[18462\] = 40902\""
        }
	tension {
	    lappend comlist "TI_DO \"C\[18462\] = 40901\""
        }
	default {
	    error "Invalid drivemode $tmode"
        }
    }
    switch $hmode {
	force {
	    lappend comlist "TI_DO \"P\[18463\] = 2\""
	}
	pos {
	    lappend comlist "TI_DO \"P\[18463\] = 1\""
        }
	tension {
	    lappend comlist "TI_DO \"P\[18463\] = 3\""
        }
	default {
	    error "Invalid holdmode $tmode"
        }
    }
    lappend comlist [format "TI_DO \"P\[18462\] = %8.5f\"" $speed]

}
#---------------------------------------------------------------------------
proc zwickroll::addAxis2Par {ax} {
    global zwickroll::comlist

    set speed [hval /sics/${ax}/speed]
    set dmode [hval /sics/${ax}/drivemode]
    set tmode [hval /sics/${ax}/targetmode]
    set hmode [hval /sics/${ax}/holdmode]
    set target [hval /sics/${ax}/targetposition]

    zwickroll::settimeout $ax $speed $dmode $tmode $target

    switch $tmode {
	force {
	    lappend comlist "TI_DO \"C\[18466\] = 27658\""
	}
	pos {
	    lappend comlist "TI_DO \"C\[18466\] = 27659\""
        }
	tension {
	    lappend comlist "TI_DO \"C\[18466\] = 27652\""
        }
	default {
	    error "Invalid targetmode $dmode"
        }
    }
    lappend comlist "TI_DO \"P\[18466\] = $target\""

    lappend comlist "TI_DO \"P\[18467\] = 0.0001\""
    switch $dmode {
	force {
	    lappend comlist "TI_DO \"C\[18467\] = 28675\""
	}
	pos {
	    lappend comlist "TI_DO \"C\[18467\] = 28674\""
        }
	tension {
	    lappend comlist "TI_DO \"C\[18467\] = 28676\""
        }
	default {
	    error "Invalid drivemode $tmode"
        }
    }
    switch $hmode {
	force {
	    lappend comlist "TI_DO \"P\[18468\] = 2\""
	}
	pos {
	    lappend comlist "TI_DO \"P\[18468\] = 1\""
        }
	tension {
	    lappend comlist "TI_DO \"P\[18468\] = 3\""
        }
	default {
	    error "Invalid holdmode $tmode"
        }
    }
    lappend comlist [format "TI_DO \"P\[18467\] = %8.5f\"" $speed]

}
#---------------------------------------------------------------------------
proc zwickroll::addAxis3Par {ax} {
    global zwickroll::comlist

    set speed [hval /sics/${ax}/speed]
    set dmode [hval /sics/${ax}/drivemode]
    set tmode [hval /sics/${ax}/targetmode]
    set hmode [hval /sics/${ax}/holdmode]
    set target [hval /sics/${ax}/targetposition]

    zwickroll::settimeout $ax $speed $dmode $tmode $target

    switch $tmode {
	force {
	    lappend comlist "TI_DO \"C\[18471\] = 27669\""
	}
	pos {
	    lappend comlist "TI_DO \"C\[18466\] = 27670\""
        }
	tension {
	    lappend comlist "TI_DO \"C\[18466\] = 27668\""
        }
	default {
	    error "Invalid targetmode $dmode"
        }
    }
    lappend comlist "TI_DO \"P\[18471\] = $target\""

    lappend comlist "TI_DO \"P\[18472\] = 0.0001\""
    switch $dmode {
	force {
	    lappend comlist "TI_DO \"C\[18472\] = 28686\""
	}
	pos {
	    lappend comlist "TI_DO \"C\[18472\] = 28684\""
        }
	tension {
	    lappend comlist "TI_DO \"C\[18472\] = 28685\""
        }
	default {
	    error "Invalid drivemode $tmode"
        }
    }
    switch $hmode {
	force {
	    lappend comlist "TI_DO \"P\[18473\] = 2\""
	}
	pos {
	    lappend comlist "TI_DO \"P\[18473\] = 1\""
        }
	tension {
	    lappend comlist "TI_DO \"P\[18473\] = 3\""
        }
	default {
	    error "Invalid holdmode $tmode"
        }
    }

    lappend comlist [format "TI_DO \"P\[18472\] = %8.5f\"" $speed]

}
#----------------------------------------------------------------------------
proc zwickroll::addAxis1Commands {} {
    global zwickroll::names zwickroll::comlist
    global zwickroll::waitlist zwickroll::waitcount

    set ax [lindex $zwickroll::names 0]
    addAxis1Par $ax 
    lappend waitlist 30736 30764
    incr waitcount

    set execode "TI_DO \"M_NextStep(310)\""

    set ax [lindex $zwickroll::names 1]
    set active [hval /sics/${ax}/active]
    if {$active == 1} {
	addAxis2Par $ax
	lappend waitlist 30738
	set waitcount 2
	set execode "TI_DO \"M_NextStep(320)\""
	lappend comlist $execode
	hupdate /sics/${ax}/active 0
	return
    }

    set ax [lindex $zwickroll::names 2]
    set active [hval /sics/${ax}/active]
    if {$active == 1} {
	addAxis3Par $ax
	lappend waitlist 30740
	set waitcount 2
	set execode "TI_DO \"M_NextStep(321)\""
	lappend comlist $execode
	hupdate /sics/${ax}/active 0
	return
    }

    lappend comlist $execode

}
#----------------------------------------------------------------------------
proc zwickroll::addAxis2Commands {} {
    global zwickroll::names zwickroll::comlist
    global zwickroll::waitlist zwickroll::waitcount

    set ax [lindex $zwickroll::names 1]
    addAxis2Par $ax 
    lappend waitlist 30738 30764
    incr waitcount

    set execode "TI_DO \"M_NextStep(311)\""

    set ax [lindex $zwickroll::names 2]
    set active [hval /sics/${ax}/active]
    if {$active == 1} {
	addAxis3Par $ax
	lappend waitlist 30740
	set waitcount 2
	set execode "TI_DO \"M_NextStep(322)\""
	lappend comlist $execode
	hupdate /sics/${ax}/active 0
	return
    }

    lappend comlist $execode
    
}
#----------------------------------------------------------------------------
proc zwickroll::addAxis3Commands {} {
    global zwickroll::names zwickroll::comlist
    global zwickroll::waitlist zwickroll::waitcount

    set ax [lindex $zwickroll::names 2]
    addAxis3Par $ax 
    lappend waitlist 30740 30764
    incr waitcount

    set execode "TI_DO \"M_NextStep(312)\""
    lappend comlist $execode

#    Further check: cannot happen because of test sequence 1,2 get tested first

}
#-------------------------------------------------------------------------
# The zwickroll uses different run commands for different combinations 
# of simultaneous axis. This is handled by a little recursive descent 
# processing. Which means that the addAxisXCommands functions check for other 
# axes to run. In order to prevent accidental, additional processing in the 
# code below, these functions have to clear the active flag of those axes 
# they processed.
#
# Which poses a problem when checking for the drivetimeout later on. 
# Thus I save the flags and reset them at the end of this procedure
#---------------------------------------------------------------------------
proc zwickroll::buildCommandList {} {
    global zwickroll::names zwickroll::comlist zwickroll::compptr

    calculateDirections

    set ax [lindex $zwickroll::names 0]
    set active [hval /sics/${ax}/active]
    lappend alist $active
    if {$active == 1} {
	addAxis1Commands
    }

    set ax [lindex $zwickroll::names 1]
    set active [hval /sics/${ax}/active]
    lappend alist $active
    if {$active == 1} {
	addAxis2Commands
    }

    set ax [lindex $zwickroll::names 2]
    set active [hval /sics/${ax}/active]
    lappend alist $active
    if {$active == 1} {
	addAxis3Commands
    }

    for {set i 0} {$i < 3} {incr i} {
	set ax [lindex $zwickroll::names $i]
	set active [lindex $alist $i]
	hupdate /sics/${ax}/active $active
    }
}
#-------------------------------------------------------------------------
proc zwickroll::printCommandList {} {
    global zwickroll::comlist 
    
    sct print "Current command list"
    foreach entry $zwickroll::comlist {
	sct print $entry
    }
}
#-------------------------------------------------------------------------
proc zwickroll::clearActive {} {
    global zwickroll::names

    foreach ax $names {
	hupdate /sics/${ax}/active 0
    }
}
#-------------------------------------------------------------------------
proc zwickroll::processtxevent {name data} {
    global zwickroll::waitlist zwickroll::waitcount
    global zwickroll::breakcodes zwickroll::limcodes
    global zwickroll::state

    if {[string first TX_EVENT $data] < 0} {
	return cont
    }
    

    set l [split $data]
    set event [string trim [lindex $l 1]]
    if {[lsearch $waitlist $event] >= 0} {
	clientput "Discovered stop event: $event"
	if {$event == 30764} {
	    set waitcount 0
	} else {
	    incr waitcount -1
	}
	switch $event {
	    30736 {
		hupdate /sics/zax1/active 0
	    }
	    30738 {
		hupdate /sics/zax2/active 0
	    }
	    30740 {
		hupdate /sics/zaxt/active 0
	    }
	}
   } 
    if {[lsearch $zwickroll::breakcodes $event] >= 0} {
	clientlog "ERROR: sample broken!!!!!!!!"
	set zwickroll::state idle
	clearActive
	setint abortbatch
	hupdate /sics/${name}/status fault
	return abort
    }
    if {[lsearch $zwickroll::limcodes $event] >= 0} {
	clientlog "ERROR: Machine limit exceeded, aborting"
	clientlog "Restart tensile machine and reset limits to clear"
	set zwickroll::state idle
	hupdate /sics/${name}/status fault
	clearActive
	setint abortbatch
	return abort
    }
    return cont
} 
#--------------------------------------------------------------------------
# This is state machine with various states:
# - starting: construct the command list to send
# - sendsend, sendrec : sends commands 
# - recsend, receiving: receives replies
# - idle     : job finished
#--------------------------------------------------------------------------
proc zwickroll::status {name} {
    global zwickroll::state zwickroll::sct 
    global zwickroll::comptr zwickroll::comlist
    global zwickroll::waitlist zwickroll::waitcount
    global zwickroll::breakcodes zwickroll::limcodes
    global zwickroll::sicsstate

    set killerrors [list 23403 23007 3037]

#    clientlog "Running zwickroll::status with state $zwickroll::state"
    set forcestop [hval /sics/${name}/forcestop]
    if {$forcestop == 1} {
	set zwickroll::state idle
	clearActive
	sct update fault
	return idle
    }
		   
    switch $zwickroll::state {
	starting {
	    buildCommandList 
	    set zwickroll::state sendsend
#	    printCommandList 
	    sct update run
	    $sct queue /sics/${name}/status progress read
        }
	sendsend {
	    if { $zwickroll::comptr < [llength $comlist]} {
		sct send [lindex $comlist $zwickroll::comptr]
		incr zwickroll::comptr
		set zwickroll::state sendrec
		return read
            } else {
		set zwickroll::state recsend
		$sct queue /sics/${name}/status progress read
		return idle
            }
	}
	sendrec {
	    set data [sct result]
	    if {[string first TX_WARNING $data] >= 0} {
		sct send @@NOSEND@@
		set zwickroll::state sendrec
		return read
            }
	    if {[string first TX_EVENT $data] >= 0} {
		sct send @@NOSEND@@
		set zwickroll::state sendrec
		return read
            }
	    if {[string first "response"  $data] >= 0} {
		sct send @@NOSEND@@
		set zwickroll::state sendrec
		return read
            }
	    if {[string length $data] < 2} {
		sct send @@NOSEND@@
		set zwickroll::state sendrec
		return read
            } 
	    if { ([string first SUCCESS $data] < 0) && ([string first TRUE $data] < 0) } {
		sct update fault
		if {[string first 1000 $data] >0 } {
		    clientlog "Tensile machine NOT running\nrun zrrig start and try again"
		    set zwickroll::waitcount 0
		    set zwickroll::state idle
                    setint abortbatch
		    $sct queue /sics/${name}/status progress read
		    return idle
                } else {
		    clientlog "Zwickroll detected error $data"
		}
		return idle
            }
	    $sct queue /sics/${name}/status progress read
	    set zwickroll::state sendsend
	    return idle
	}
	recsend {
#------------------- see comment in poldi.tcl concerning zwstatus
	    if {$zwickroll::sicsstate > -1} {
		sct send "TI_DO \"M_SetState($zwickroll::sicsstate)\""
		set zwickroll::sicsstate -1
		set zwickroll::state receiving
		return read
            }
#-------------- send position update requests any now and then
	    set upcount [sct upcount]
	    if {$upcount < 2} {
		sct send @@NOSEND@@
		incr upcount
		sct upcount $upcount
	    } else {
		zwickroll::read $name
		sct upcount 0
            }
	    set zwickroll::state receiving
	    return read
        }
	receiving {
	    set data [sct result]
	    if {[string first TX_EVENT $data] >= 0} {
		set stat [processtxevent $name $data]
		if {[string compare $stat abort] == 0} {
		    sct update fault
		    return idle
                } elseif {$zwickroll::waitcount == 0} {
		    set zwickroll::state finish
                } else {
		    set zwickroll::state recsend
		}
		append zwickroll::replydata $data \n
		$sct queue /sics/${name}/status progress read
	    } elseif {[string first TI_ERR_A $data] >= 0} {
		set l [split $data]
		set errcode [lindex $l 1]
		clientlog "Zwickroll reported error: $errcode"
		if {[lsearch $killerrors $errcode] >= 0} {
		    clientlog "Critical error: aborting... "
		    set zwickroll::state idle
		    set waitcount 0
		    setint abortbatch
		    sct update idle
		    clearActive
		    return idle
                } else {
		    incr waitcount -1
		    if {$zwickroll::waitcount <= 0} {
			set zwickroll::state finish
		    }
                }
		set zwickroll::state recsend
		$sct queue /sics/${name}/status progress read
	    } elseif {[string first TI_GETN_A $data] >= 0} {
		set val [string trim [string range $data 9 end]]
		hupdate /sics/${name} $val
		set dmode [hval /sics/${name}/drivemode]
		switch $dmode {
		    force {
			hupdate /sics/${name}/forcepos $val
		    }
		    pos {
			hupdate /sics/${name}/pos $val
		    }
		    tension {
			hupdate /sics/${name}/strainpos $val
		    }
		}
		set zwickroll::state recsend
		$sct queue /sics/${name}/status progress read
            } else {
		zwickroll::checktimeout
		if {$zwickroll::waitcount <= 0} {
		    set zwickroll::state finish
		    clientlog "Driving finished on timeout rather then on event" 
                } else {
		    append zwickroll::replydata $data \n
		    set zwickroll::state recsend
		}
		$sct queue /sics/${name}/status progress read
	    }
	    return idle
        }
	finish {
	    set zwickroll::state finishrec
	    zwickroll::read $name
	    return read
        }
	finishrec {
	    set zwickroll::state idle
	    set data [sct result]
	    if {[string first TI_GETN_A $data] < 0} {
		processtxevent $name $date
		sct send @@NOSEND@@
		return read
	    }
	    set val [string trim [string range $data 9 end]]
	    hupdate /sics/${name} $val
	    clearActive
	    sct update idle
	    return idle
        }
	idle {
	    clearActive
	    sct update idle
	    return idle
        }
	default {
	    error "Unknown state $zwickroll::state"
        }
    }

    return idle
}
#--------------------------------------------------------------------------
proc zwickroll::stop {} {
    global zwickroll::sct
    $zwickroll::sct send "TI_DO \"M_NextStep(120)\""
    return idle
}
#--------------------------------------------------------------------------
proc zwickroll::write {name} {
    global zwickroll::state zwickroll::sct
    hupdate /sics/${name}/targetposition [sct target]
    hupdate /sics/${name}/active 1
    set zwickroll::state starting

    hupdate /sics/${name}/status run
    hupdate /sics/${name}/forcestop 0 

    $sct queue /sics/${name}/status progress read
    sct writestatus done
    clientput "zwickroll::write executed"
    return idle
}
#---------------------------------------------------------------------------
proc zwickroll::checkstatus {name} {
    return [hval /sics/${name}/status]
}
#---------------------------------------------------------------------------
proc zwickroll::null {mot} {
    global zwickroll::names zwickroll::sct
    set codes [list 18451 18452 18453]

    for {set i 0} {$i < [llength $names]} {incr i} {
	set ax [lindex $names $i]
	if {[string compare $ax $mot] == 0 } {
	    set com [format "TI_DO \"B\[%d\]= TRUE\"" [lindex $codes $i]]
	}  else {
	    set com [format "TI_DO \"B\[%d\]= FALSE\"" [lindex $codes $i]]
	}
	$sct transact $com
    }
    $sct transact "TI_DO \"M_NextStep(230)\"" 
}
#---------------------------------------------------------------------------
proc zwickroll::getlim {mot mode} {
    global zwickroll::limits
    
    set status [catch {set v $limits(/${mot}/${mode})} msg]
    if {$status == 0} {
	return $v
    } else {
	return -9999
    }
}
#--------------------------------------------------------------------------
proc zwickroll::setlim {mot mode val} {
    global zwickroll::limits zwickroll::sct
    global zwickroll::modes zwickroll::names
    
    set limcode [list 18492 18495 18498]
    set limstep [list 410 412 414]
    set modecodes(0) [list 40408 40412 40409]
    set modecodes(1) [list 27658 27649 27659]
    set modecodes(2) [list 27669 27673 27670]
    
    set midx [lsearch $modes $mode]
    set motidx [lsearch $names $mot]
    
    set code [lindex $limcode $motidx]
    set codeval [lindex $modecodes($motidx) $midx]
    $sct transact [format "TI_DO \"C\[%d\] = %d\"" $code $codeval]
    $sct transact [format "TI_DO \"P\[%d\] = %f\"" $code $val]
    set actioncode [lindex $limstep $motidx]
    $sct transact [format "TI_DO \"M_NextStep(%d)\"" $actioncode]
    set zwickroll::limits(/${mot}/${mode}) $val
}
#-------------------------------------------------------------------------
proc zwickroll::break {mot value} {
    global zwickroll::names zwickroll::sct

    set parcodes [list 18501 18504 18507]
    set actioncodes [list 510 512 514]

    set motidx [lsearch $names $mot]
    set parcode [lindex $parcodes $motidx]
    set actioncode [lindex $actioncodes $motidx]

    $zwickroll::sct transact [format "TI_DO \"PU\[%d\] = %d\"" $parcode $value]
    $sct transact [format "TI_DO \"M_NextStep(%d)\"" $actioncode]
    return OK
}
#--------------------------------------------------------------------------
proc zwickroll::channelformot {mot mode} {
   global zwickroll::names zwickroll::modes

    set axID [lsearch $names $mot]
    set modeID [lsearch $modes $mode]
    switch $axID {
	0 {
	    set chanlist [list 40408 40409 40407]
        }
	1 {
	    set chanlist [list 27658 27659 27652]
	}
	2  {
	    set chanlist [list 27670 27669 27668]
	}
	default {
	    error "Invalid axis code $axID"
	}
    }
    set chan [lindex $chanlist $modeID]
    return $chan

}
#---------------------------------------------------------------------------
# This is again a little state machine looping through the modes with property 
# midx as the mode counter. The point is to update everything which can be 
# updated. The actual value is determined by targetmode. 
#----------------------------------------------------------------------------
proc zwickroll::read {name} {
    global zwickroll::names zwickroll::modes

    set midx [sct midx]
    set mode [lindex $modes $midx]

    set chan [zwickroll::channelformot $name $mode]

    set com [format "TI_GETN \"ChannelValue(%s)\"" $chan]
    sct send $com
    return readreply
}
#---------------------------------------------------------------------------
proc zwickroll::readreply {name} {
    global zwickroll::modes

    set data [sct result]
    if {[string first TX_EVENT $data] >= 0} {
	processtxevent $name $data
	sct send @@NOSEND@@
	return readreply
    }
    if {[string first TI_DO_A $data] >= 0} {
	sct send @@NOSEND@@
	return readreply
    }
    if {[string first TI_GETN_A $data] < 0} {
	error "Invalid read response $data"
    }
    set val [string trim [string range $data 9 end]]

    set tmode [hval /sics/${name}/targetmode]
    set tidx [lsearch $modes $tmode]

    set midx [sct midx]
    if {$tidx == $midx} {
	sct update $val
    }

    switch $midx {
	0 {
	    hupdate /sics/${name}/forcepos $val
	    sct midx 1
	    return read
	}
	1 {
	    hupdate /sics/${name}/pos $val
	    sct midx 2
	    return read
	}
	2 {
	    hupdate /sics/${name}/strainpos $val
	    sct midx 0
	    return idle
        }
	default {
	    sct midx 0
	    return idle
	}
    }
}
#--------------------------------------------------------------------------
proc zwickroll::sysstatus {} {
    global zwickroll::sct

    set response [$sct transact TI_ISMACHINEREADY]
    if {[string first YES $response] >= 0 } {
	return on
    } else {
	return off
    }
}
#--------------------------------------------------------------------------
proc zwickroll::setsys {value} {
    global zwickroll::sct

    switch $value {
	start {
	    $sct transact TI_START
	    wait 5
	}
	stop {
	    $sct transact TI_STOP
	}
	default {
	    
	    error "Only start/stop allowed, cannot use $value"
	}
    }
}
#--------------------------------------------------------------------------
proc zwickroll::channelforlog {mot mode} {
   global zwickroll::names zwickroll::modes

    if {$mode == "time"} {
	return 40400
    }

    set axID [lsearch $names $mot]
    set modeID [lsearch $modes $mode]
    switch $axID {
	0 {
	    set chanlist [list 40402 40401 40403]
        }
	1 {
	    set chanlist [list 27653 27650 27657]
	}
	2  {
	    set chanlist [list 27665 27664 27666]
	}
	default {
	    error "Invalid axis code $axID"
	}
    }
    set chan [lindex $chanlist $modeID]
    return $chan

}
#-------------------------------------------------------------------------
proc zwickroll::configurelog {} {
    global zwickroll::sct zwickroll:names zwickroll::modes
    global zwickroll::log1 zwickroll::log2 zwickroll::log3

    set state [lindex $zwickroll::log1 0]
    set sen [lindex $zwickroll::log1 1]
    set intervall [lindex $zwickroll::log1 2]
    set mode [string range $sen 0 end-1]
    set motno [string range $sen end end]
    incr motno -1

    set mot [lindex $zwickroll::names $motno]
    if {[string first on $state] < 0} {
	$sct transact "TI_DO \"B\[18454\] = False\""
    } else {
	set chan [zwickroll::channelforlog $mot $mode]
	$sct transact "TI_DO \"B\[18454\] = True\""
	$sct transact "TI_DO \"C\[18454\] = $chan\""
	$sct transact "TI_DO \"P\[18454\] = $intervall\""
    }

    set state [lindex $zwickroll::log2 0]
    set sen [lindex $zwickroll::log2 1]
    set intervall [lindex $zwickroll::log2 2]
    set mode [string range $sen 0 end-1]
    set motno [string range $sen end end]  
    incr motno -1

    set mot [lindex $zwickroll::names $motno]
    if {[string first on $state] < 0} {
	$sct transact "TI_DO \"B\[18455\] = False\""
    } else {
	set chan [zwickroll::channelforlog $mot $mode]
	$sct transact "TI_DO \"B\[18455\] = True\""
	$sct transact "TI_DO \"C\[18455\] = $chan\""
	$sct transact "TI_DO \"P\[18455\] = $intervall\""
    }

    set state [lindex $zwickroll::log3 0]
    set sen [lindex $zwickroll::log3 1]
    set intervall [lindex $zwickroll::log3 2]
    set mode [string range $sen 0 end-1]
    set motno [string range $sen end end]  
    incr motno -1

    set mot [lindex $zwickroll::names $motno]
    if {[string first on $state] < 0} {
	$sct transact "TI_DO \"B\[18456\] = False\""
    } else {
	set chan [zwickroll::channelforlog $mot $mode]
	$sct transact "TI_DO \"B\[18456\] = True\""
	$sct transact "TI_DO \"C\[18456\] = $chan\""
	$sct transact "TI_DO \"P\[18456\] = $intervall\""
    }

    $sct transact "TI_DO \"M_NextStep(210)\""
}
#---------------------------------------------------------------------------
proc zwickroll::startcyclic {motobj mot} {
    global zwickroll::names zwickroll::sct


    set idx [lsearch $names $mot]
    set mode [hval /sics/${motobj}/mode]
    set channel [zwickroll::channelformot $mot $mode]
    hupdate /sics/${motobj}/status run

    set ampcodes [list 18511 18519 18528]
    set amp [hval /sics/${motobj}/amplitude]
    set code [lindex $ampcodes $idx]
    $sct transact "TI_DO \"C\[$code\]=$channel\"" 
    $sct transact "TI_DO \"P\[$code\]=$amp\"" 

    set offcodes [list 18512 18520 18529]
    set off [hval /sics/${motobj}/offset]
    set code [lindex $offcodes $idx]
    $sct transact "TI_DO \"C\[$code\]=$channel\"" 
    $sct transact "TI_DO \"P\[$code\]=$off\"" 

    set freqcodes [list 18513 18521 18530]
    set freq [hval /sics/${motobj}/frequency]
    set code [lindex $freqcodes $idx]
    $sct transact "TI_DO \"P\[$code\]=$freq\"" 
	     
    set nccodes [list 18515 18523 18532]
    set nc [hval /sics/${motobj}/ncycles]
    set code [lindex $nccodes $idx]
    $sct transact "TI_DO \"P\[$code\]=$nc\"" 

    set velcodes [list 18518 18527 18535]
    set vel [hval /sics/${motobj}/velocity]
    set code [lindex $velcodes $idx]
    set chancodes [list 40902 28674 28684]
    set channel [lindex $chancodes $idx]
    $sct transact "TI_DO \"P\[$code\]=0.0001\"" 
    $sct transact "TI_DO \"C\[$code\]=$channel\"" 
    $sct transact "TI_DO \"P\[$code\]=$vel\"" 

    set startcodes [list 330 331 332 18527]
    set code [lindex $startcodes $idx]
    $sct transact "TI_DO \"M_NextStep($code)\"" 

    $sct queue /sics/${motobj}/status progress read
}
#---------------------------------------------------------------------------
proc zwickroll::readcyclic {} {
    sct send @@NOSEND@@
    return cyclicresponse
}
#--------------------------------------------------------------------------
proc zwickroll::cyclicresponse {name} {

    set waitlist [list 30746 30748 30750 40602]
    set data [sct result]
    if {[string first TX_EVENT $data] >= 0} {
	set l [split $data]
	set event [string trim [lindex $l 1]]
	if {[lsearch $waitlist $event] >= 0} {
	    clientput "Discovered cyclic stop event: $event"
	    sct update idle
	    return idle
	} 
    }
    if {[string first TI_ERR $data] >= 0} {
	sct print "Error: $data in cyclic"
	sct update idle
	return idle
    }
    if {[string first TX_ERROR $data] >= 0} {
	sct print "Error: $data in cyclic"
	sct update idle
	return idle
    }
    $zwickroll::sct queue /sics/${name}/status progress read
    return idle
}
#---------------------------------------------------------------------------
proc zwickroll::showlog {} {
    global zwickroll::log1 zwickroll::log2 zwickroll::log3
    clientput "State   Sensor   Intervall"
    set state [lindex $zwickroll::log1 0]
    set sens  [lindex $zwickroll::log1 1]
    set intervall [lindex $zwickroll::log1 2]
    clientput [format "%6s%9s%12.2f" $state $sens $intervall]
    set state [lindex $zwickroll::log2 0]
    set sens  [lindex $zwickroll::log2 1]
    set intervall [lindex $zwickroll::log2 2]
    clientput [format "%6s%9s%12.2f" $state $sens $intervall]
    set state [lindex $zwickroll::log3 0]
    set sens  [lindex $zwickroll::log3 1]
    set intervall [lindex $zwickroll::log3 2]
    clientput [format "%6s%9s%12.2f" $state $sens $intervall]
}
#---------------------------------------------------------------------------
proc zwickroll::setlog1 {state sensor intervall} {
    global zwickroll::log1

    set allowedStates [list on off]
    set allowedSensors [list force1 force2 force3 pos1 pos2 pos3 tension1 tension2 tension3 time1 time2 time3]
    if {[lsearch $allowedStates $state] < 0} {
	error "State $state disallowed, only understand on/off"
    }
    if {[lsearch $allowedSensors $sensor] < 0} {
	set txt [join $allowedSensors]
	error "Sensor $sensor disallowed, only understand $txt"
    }
    set zwickroll::log1 [list $state $sensor $intervall]
    return OK
}
#---------------------------------------------------------------------------
proc zwickroll::setlog2 {state sensor intervall} {
    global zwickroll::log2

    set allowedStates [list on off]
    set allowedSensors [list force1 force2 force3 pos1 pos2 pos3 tension1 tension2 tension3 time1 time2 time3]
    if {[lsearch $allowedStates $state] < 0} {
	error "State $state disallowed, only understand on/off"
    }
    if {[lsearch $allowedSensors $sensor] < 0} {
	set txt [join $allowedSensors]
	error "Sensor $sensor disallowed, only understand $txt"
    }
    set zwickroll::log2 [list $state $sensor $intervall]
    return OK
}
#---------------------------------------------------------------------------
proc zwickroll::setlog3 {state sensor intervall} {
    global zwickroll::log3

    set allowedStates [list on off]
    set allowedSensors [list force1 force2 force3 pos1 pos2 pos3 tension1 tension2 tension3 time1 time2 time3]
    if {[lsearch $allowedStates $state] < 0} {
	error "State $state disallowed, only understand on/off"
    }
    if {[lsearch $allowedSensors $sensor] < 0} {
	set txt [join $allowedSensors]
	error "Sensor $sensor disallowed, only understand $txt"
    }
    set zwickroll::log3 [list $state $sensor $intervall]
    return OK
}
#---------------------------------------------------------------------------
proc zwickroll::runno {no} {
    global zwickroll::sct
    $sct transact "TI_DO \"M_SetRun($no)\"" 
}
#---------------------------------------------------------------------------
proc zwickroll::setstate {no} {
    global zwickroll::sct
    $sct transact "TI_DO \"M_SetState($no)\"" 
}
#---------------------------------------------------------------------------
proc zwickroll::makezwickroll {mysct} {
    global zwickroll::sct zwickroll:names zwickroll::modes
    
    set zwickroll::sct $mysct

    foreach mot  $zwickroll::names {
	makesctdriveobj $mot float user ZwickRoll $sct
        hsetprop /sics/${mot} fmt "%6.2f"
	hfactory /sics/${mot}/targetposition plain user float
	hset /sics/${mot}/targetposition 0
        hsetprop /sics/${mot}/targetposition fmt "%6.2f"
	hfactory /sics/${mot}/active plain user int
	hset /sics/${mot}/active 0
	hfactory /sics/${mot}/speed plain user float
	hset /sics/${mot}/speed .001
	hsetprop /sics/${mot}/speed __save true
        hsetprop /sics/${mot}/speed fmt "%8.5f"

	hfactory /sics/${mot}/drivemode plain user text
	hset /sics/${mot}/drivemode pos
	hsetprop /sics/${mot}/drivemode values [join $zwickroll::modes ,]
	haddcheck /sics/${mot}/drivemode values
	hsetprop /sics/${mot}/drivemode __save true
	hfactory /sics/${mot}/targetmode plain user text
	hset /sics/${mot}/targetmode force
	hsetprop /sics/${mot}/targetmode values [join $zwickroll::modes ,]
	haddcheck /sics/${mot}/targetmode values
	hsetprop /sics/${mot}/targetmode __save true
	hfactory /sics/${mot}/holdmode plain user text
	hset /sics/${mot}/holdmode force
	hsetprop /sics/${mot}/holdmode values [join $zwickroll::modes ,]
	haddcheck /sics/${mot}/holdmode values
	hsetprop /sics/${mot}/holdmode __save true
	hsetprop /sics/${mot} checklimits zwickroll::check $mot
	hsetprop /sics/${mot} checkstatus zwickroll::checkstatus $mot
	hsetprop /sics/${mot} halt zwickroll::stop
	hsetprop /sics/${mot} write zwickroll::write $mot
	hfactory /sics/${mot}/status plain user text
	hupdate /sics/${mot}/status idle
	hsetprop /sics/${mot}/status read zwickroll::status $mot
	hsetprop /sics/${mot}/status upcount 0
	hfactory /sics/${mot}/forcestop plain user int
	hupdate /sics/${mot}/forcestop 0
	hfactory /sics/${mot}/forcepos plain internal float
        hsetprop /sics/${mot}/forcepos fmt "%8.2f"
	hfactory /sics/${mot}/pos plain internal float
        hsetprop /sics/${mot}/pos fmt "%8.2f"
	hfactory /sics/${mot}/strainpos plain internal float
        hsetprop /sics/${mot}/strainpos fmt "%8.2f"
	hfactory /sics/${mot}/drivetimeout plain internal int


	$mot makescriptfunc null "zwickroll::null $mot" user
	$mot makescriptfunc breaklim "zwickroll::break $mot" user
	hfactory /sics/${mot}/breaklim/limit plain user int
	
	foreach md $zwickroll::modes {
	    hfactory /sics/${mot}/${md}limit script \
		"zwickroll::getlim $mot $md" "zwickroll::setlim $mot $md" float
	    hsetprop /sics/${mot}/${md}limit priv user
        }

        hsetprop /sics/${mot} read zwickroll::read $mot
        hsetprop /sics/${mot} readreply  zwickroll::readreply $mot 
        hsetprop /sics/${mot} midx 0
        hsetprop /sics/${mot}/status midx 0
	$mysct poll /sics/${mot} 60
	$mysct write /sics/${mot}
    }

    MakeSicsObj zrrig ZwickRollRig
    zrrig makescriptfunc start "zwickroll::setsys start" user
    zrrig makescriptfunc stop "zwickroll::setsys stop" user
    zrrig makescriptfunc showlog "zwickroll::showlog" spy

    zrrig makescriptfunc setlog1 "zwickroll::setlog1 " user
    hfactory /sics/zrrig/setlog1/state plain user text
    hfactory /sics/zrrig/setlog1/sensor plain user text
    hfactory /sics/zrrig/setlog1/intervall plain user float
    zrrig makescriptfunc setlog2 "zwickroll::setlog2 " user
    hfactory /sics/zrrig/setlog2/state plain user text
    hfactory /sics/zrrig/setlog2/sensor plain user text
    hfactory /sics/zrrig/setlog2/intervall plain user float
    zrrig makescriptfunc setlog3 "zwickroll::setlog3 " user
    hfactory /sics/zrrig/setlog3/state plain user text
    hfactory /sics/zrrig/setlog3/sensor plain user text
    hfactory /sics/zrrig/setlog3/intervall plain user float
    zrrig makescriptfunc setrunno "zwickroll::runno " user
    hfactory /sics/zrrig/setrunno/no plain user int
    zrrig makescriptfunc setstate "zwickroll::setstate " user
    hfactory /sics/zrrig/setstate/no plain user int


    set v2 [concat $zwickroll::modes none]
    foreach mot $zwickroll::names {
	hfactory /sics/zrrig/log${mot} plain user text
	hupdate /sics/zrrig/log${mot} force
	hsetprop /sics/zrrig/log${mot} values [join $v2 ,]
	haddcheck /sics/zrrig/log${mot} values
	hfactory /sics/zrrig/log${mot}diff plain user float
	hupdate /sics/zrrig/log${mot}diff 2
    }
    zrrig makescriptfunc configurelog zwickroll::configurelog user

    foreach mot $zwickroll::names {
	set motobj ${mot}c
	MakeSicsObj $motobj ZwickRollCyclic
	hfactory /sics/${motobj}/mode plain user text
	hupdate /sics/${motobj}/mode force
	hsetprop /sics/${motobj}/mode values [join zwickroll::modes ,]
	haddcheck /sics/${motobj}/mode values
	hsetprop /sics/${motobj}/mode __save true
	hfactory /sics/${motobj}/amplitude plain user float
	hupdate /sics/${motobj}/amplitude 10
	hsetprop /sics/${motobj}/amplitude __save true
        hsetprop /sics/${motobj}/amplitude fmt "%8.2f"
	hfactory /sics/${motobj}/offset plain user float
	hupdate /sics/${motobj}/offset 10
        hsetprop /sics/${motobj}/offset fmt "%8.2f"
	hsetprop /sics/${motobj}/offset __save true
	hfactory /sics/${motobj}/frequency plain user float
	hupdate /sics/${motobj}/frequency .5
	hsetprop /sics/${motobj}/frequency __save true
        hsetprop /sics/${motobj}/amplitude fmt "%8.2f"
	hfactory /sics/${motobj}/ncycles plain user int
	hupdate /sics/${motobj}/ncycles 10
	hsetprop /sics/${motobj}/ncycles __save true
	hfactory /sics/${motobj}/velocity plain user float
	hupdate /sics/${motobj}/velocity 1
	hsetprop /sics/${motobj}/velocity __save true
        hsetprop /sics/${motobj}/velocity fmt "%8.2f"
	hfactory /sics/${motobj}/status plain user text
	hupdate /sics/${motobj}/status idle
	hsetprop /sics/${motobj}/status read zwickroll::readcyclic
	hsetprop /sics/${motobj}/status cyclicresponse \
	    "zwickroll::cyclicresponse $motobj"
	$motobj makescriptfunc start "zwickroll::startcyclic $motobj $mot" user 
	$motobj makescriptfunc stop "zwickroll::stop" user 
    }
#    zrrig start
}
 

