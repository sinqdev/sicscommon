#---------------------------------------------------------------
# This is a second generation simulation motor. 
#
# copyright: see file COPYRIGHT
#
# Mark Koennecke, December 2008
#
# Added a regression driver for the second generation motor
#
# Mark Koennecke, July 2013
#----------------------------------------------------------------
proc simhardset {motname newval} {
    hset /sics/$motname/starttime [clock sec]
    hupdate /sics/$motname/oldpos $newval
}
#--------------------------------------------------------------
proc simhardget {motname} {
    set stat [hval /sics/$motname/status]
    set val [hval /sics/$motname/targetposition]
    if {[string first run $stat] >= 0 \
	    || [string first error $stat] >= 0 } {
	return [expr $val -.777]
    } else {
	return $val
    }
}
#-------------------------------------------------------------
proc simhardfaultget {motname} {
    set val [hval /sics/$motname/targetposition]
    return [expr $val - .2]
}
#--------------------------------------------------------------
proc simstatusget {motname} {
    set start [hval /sics/$motname/starttime]
    if {$start < 0} {
	return error
    }
    set delay [hval /sics/$motname/delay]
    if {[clock sec] > $start + $delay} {
	set val [hval /sics/$motname/targetposition]
	hupdate /sics/${motname}/hardposition $val
	return idle
    } else {
	return run
    }
}
#-------------------------------------------------------------
proc simstatusfault {motname } {
    clientput "ERROR: I am feeling faulty!" 
    return error
}
#--------------------------------------------------------------
proc simhalt {motname} {
    hset /sics/$motname/starttime -100
}
#---------------------------------------------------------------
proc MakeSecSim {name lower upper delay} {
    MakeSecMotor $name
    hfactory /sics/$name/delay plain user text
    hfactory /sics/$name/starttime plain user int
    hset /sics/$name/delay $delay
    hdel /sics/$name/hardposition
    hfactory /sics/$name/hardposition script "simhardget $name" "simhardset $name" float 
#    hfactory /sics/$name/hardposition script "simhardfaultget $name" "simhardset $name" float 
    hdel /sics/$name/status
    hfactory /sics/$name/status script "simstatusget $name" "hdbReadOnly b" text 
#    hfactory /sics/$name/status script "simstatusfault $name" "hdbReadOnly b" text 
    $name makescriptfunc halt "simhalt $name" user
    hupdate /sics/$name/hardupperlim $upper
    hupdate /sics/$name/softupperlim $upper
    hupdate /sics/$name/hardlowerlim $lower
    hupdate /sics/$name/softlowerlim $lower
    hfactory /sics/$name/oldpos plain user float
}
#------------------------------------------------------------------
proc regresshardget {name} {
    set errortype [hval /sics/$name/errortype]
    set recover [hval /sics/$name/recover]
    hdelprop /sics/$name geterror
    switch $errortype {
	5 {
	    if {$recover == 1} {
		hupdate /sics/$name/errortype 0
		clientlog "WARNING: fixing Failed to read $name in sim" 
		return [simhardget $name]
            }
	    hsetprop /sics/$name geterror "cannot fix: Failed to read $name in sim" 
	    clientlog "ERROR: cannot fix: Failed to read $name in sim" 
	    return "-99999"
	}
	2 -
	3 -
	4  {
	    return [simhardfaultget $name]
        }
	default {
	    return [simhardget $name]
	}
    }
}
#----------------------------------------------------------------
proc regresshardset {name value} {
    set errortype [hval /sics/$name/errortype]
    set recover [hval /sics/$name/recover]

    if {$errortype == 1} {
	if {$recover == 1} {
	    hupdate /sics/$name/errortype 0
	    hupdate /sics/$name/status restart
	    clientlog "WARNING: restarting motor $name"
        } else {
	    hupdate /sics/$name/status fault
	    set oldval [hval /sics/$name/oldpos]
	    hupdate /sics/$name/targetposition $oldval
	    error "cannot start device $name"
	}
    }
    return [simhardset $name $value]
}
#---------------------------------------------------------------
proc regressstatusget {name} {
    set errortype [hval /sics/$name/errortype]
    set recover [hval /sics/$name/recover]
    
    switch $errortype {
	3 {
	    if {$recover == 1} {
		hupdate /sics/$name/errortype 0
		clientput "WARNING: Hardware error simulated and fixed"
		return restart
            } else {
		clientput "ERROR: Hardware error simulated"
		hupdate /sics/$name/error "Hardware is mad"
		return "error"
	    }
        }
	2 {
	    clientlog "Position not reached"
	    if {$recover == 1} {
		hupdate /sics/$name/errortype 0
		return poserror
            } else {
		return "error"
	    }
        }
	4 {
	    set target [hval /sics/$name/targetposition]
	    hupdate /sics/$name/targetposition [expr $target - .2]
	    return idle
        }
	6 {
	    return run
        }
	default {
	    return [simstatusget $name]
	}
    }
    
}
#----------------------------------------------------------------
proc MakeSecRegress {name lower upper delay} {
    MakeSecSim $name $lower $upper $delay

    hdel /sics/$name/hardlowerlim
    hfactory /sics/$name/hardlowerlim plain user float
    hupdate /sics/$name/hardlowerlim $lower
    hdel /sics/$name/hardupperlim
    hfactory /sics/$name/hardupperlim plain user float
    hupdate /sics/$name/hardupperlim $upper

    hfactory /sics/$name/recover plain user int
    hfactory /sics/$name/errortype plain user int
    hupdate /sics/$name/recover 1
    hupdate /sics/$name/errortype 0
    hdel /sics/$name/hardposition
    hfactory /sics/$name/hardposition script "regresshardget $name" \
	"regresshardset $name" float 
    hdel /sics/$name/status
    hfactory /sics/$name/status script "regressstatusget $name" "hdbReadOnly b" text 
}
