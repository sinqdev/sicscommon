proc nextBackupTime {now period last} {
  upvar $last l
  set l [expr $now / $period * $period]
  return [expr $l + $period]
}

proc backupCron {path {minutes 10} {days 1}} {
  global next_backup
  set now [clock seconds]
  set minutes [expr $minutes * 60]
  set days [expr $days * 24*3600]
  if {! [info exists next_backup]} {
    set next_backup(min) [nextBackupTime $now $minutes last] 
    set next_backup(day) [nextBackupTime $now $days last]
    set file [clock format $now -format "$path/backupd-%m-%d.tcl"]
    if {![file exists $file]} {
      backup $file
    }
  }
  if {$now > $next_backup(min)} {
    set next_backup(min) [nextBackupTime $now $minutes last] 
    set file [clock format $last -format "$path/backup-%Hh%M.tcl"]
  } else {
    return 1
  }
  if {$now > $next_backup(day)} {
    set next_backup(day) [nextBackupTime $now $days last]
    set file [clock format $last -format "$path/backupd-%m-%d.tcl"]
  }
  backup $file
  return 1
}
