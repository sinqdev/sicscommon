#-----------------------------------------------------------------------------------------
# This is a connector between the Andor Ikon-M camera run through an EPICS area 
# detector IOC and SICS. 
#
# COPYRIGHT: see file COPYRIGHT
#
# Mark Koennecke, February 2015
#-----------------------------------------------------------------------------------------

namespace eval epicsccd {}

proc epicsccd::makeRWintpar {name parlist campref} {
    foreach par $parlist {
	set path /sics/${name}/${par}
	hfactory $path plain user int
	epicsadapter connectwrite $path ${campref}${par}
	epicsadapter connectread $path ${campref}${par}_RBV
	hsetprop $path __save true
    }
}
#----------------------------------------------------------------------------------------
proc epicsccd::makeRintpar {name parlist campref} {
    foreach par $parlist {
	set path /sics/${name}/${par}
	hfactory $path plain user int
	epicsadapter connectread $path ${campref}${par}
    }
}
#------------------------------------------------------------------------------------------
proc epicsccd::makeRWfloatpar {name parlist campref} {
    foreach par $parlist {
	set path /sics/${name}/${par}
	hfactory $path plain user float
	epicsadapter connectwrite $path ${campref}${par}
	epicsadapter connectread $path ${campref}${par}_RBV
	hsetprop $path __save true
    }
}
#----------------------------------------------------------------------------------------
proc epicsccd::makeRfloatpar {name parlist campref} {
    foreach par $parlist {
	set path /sics/${name}/${par}
	hfactory $path plain user float
	epicsadapter connectread $path ${campref}${par}
    }
}
#----------------------------------------------------------------------------------------
proc epicsccd::control {name} {
    set target [sget /sics/${name}/control]
    switch $target {
	1000 {
	    set p [sget /sics/${name}/preset]
	    sput /sics/${name}/AcquirePeriod $p
	    sput /sics/${name}/Acquire 1
	}
	1001 {
	    sput /sics/${name}/Acquire 0
	    idle $name
	}
	1002 {}
	1003 {}
    }
} 
#---------------------------------------------------------------------------------------
proc epicsccd::updatedim {name} {
    lappend dlist [sget /sics/${name}/ArraySizeX_RBV] [sget /sics/${name}/ArraySizeY_RBV]
    hupdate /sics/${name}/dim $dlist
}
#---------------------------------------------------------------------------------------
proc epicsccd::idle {name} {
    sput /sics/${name}/status idle
}
#---------------------------------------------------------------------------------------
proc epicsccd::finish {name} {
    set v [sget /sics/${name}/Acquire]
    if {$v == 0} {
	hupdate /sics/${name}/status idle
    }
}
#----------------------------------------------------------------------------------------
proc epicsccd::makeandor {name prefix camera} {
    MakeSecHM $name 2

    append campref $prefix $camera 

    set rwintlist [list BinX BinY MinX MinY ReverseX ReverseY SizeX SizeY Acquire \
		       AndorCooler TriggerMode AndorPreAmpGain \
		       AndorADCSpeed ImageMode NumImages NumExposures]
    makeRWintpar $name $rwintlist $campref
 
    set path /sics/hm/AndorShutterMode
    hfactory $path plain user int
    epicsadapter connectread $path ${campref}AndorShutterMode
    epicsadapter connectwrite $path ${campref}AndorShutterMode

    set path /sics/hm/RunNumber
    hfactory $path plain user int
    epicsadapter connectread $path ${campref}RunNumber
    epicsadapter connectwrite $path ${campref}RunNumber

    set rintlist [list ArraySizeX_RBV ArraySizeY_RBV ]
    makeRintpar $name $rintlist $campref

    set rwflist [list AcquireTime AcquirePeriod  \
		    ShutterOpenDelay ShutterCloseDelay Temperature]
    makeRWfloatpar $name $rwflist $campref

    set rflist [list TemperatureActual]
    makeRfloatpar $name $rflist $campref

    hscriptnotify /sics/${name}/control "epicsccd::control $name"
    hscriptnotify /sics/${name}/ArraySizeX_RBV "epicsccd::updatedim $name"
    hscriptnotify /sics/${name}/ArraySizeY_RBV "epicsccd::updatedim $name"

    epicsadapter connectread /sics/${name}/data ${prefix}image1:ArrayData

    hscriptnotify /sics/${name}/Acquire "epicsccd::finish $name"

    updatedim $name
}

