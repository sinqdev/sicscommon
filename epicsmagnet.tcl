#----------------------------------------------------------------------------
# A magnet driver going through EPICS
#
# Mark Koennecke, March 2017
#---------------------------------------------------------------------------
namespace eval epicsmagnet {}

#-------------------------------------------------------
proc epicsmagnet::stdstatus {name} {
  set test [catch {sct geterror} errortxt]
  if {$test == 0} {
    return error
  }
  set stop [hval /sics/${name}/stop]
  if {$stop == 1} {
    return error
  }
  set target [hget /sics/${name}/target]
  set tol [hval /sics/${name}/tolerance]
  set is [hval /sics/${name}]
  if {abs($target - $is) < $tol} {
    return idle
  } else {
    return run
  }
}
#------------------------------------------------------------
proc epicsmagnet::make {name hilim lowlim pvread pvwrite} {
    makesctcontroller ${name}sct testprot
    stddrive::makestddrive $name EpicsMagnet ${name}sct
    hdelprop /sics/${name} read
    hdelprop /sics/${name} write
    ${name}sct unpoll /sics/${name}
    hupdate /sics/${name}/upperlimit $hilim
    hupdate /sics/${name}/lowerlimit $lowlim
    hfactory /sics/${name}/target plain user float
    hsetprop /sics/${name} checkstatus epicsmagnet::stdstatus $name
    epicsadapter connectread /sics/${name} $pvread
    epicsadapter connectwrite /sics/${name}/target $pvwrite
}
