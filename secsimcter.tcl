#-------------------------------------------------
# Second generation Hipadaba simulation counter
#
# Mark Koennecke, July 2012
#
# Updated to add regression functionality
#
# Mark Koennecke, July 2013 
#-------------------------------------------------

namespace eval simcounter {} 

proc simcounter::values {name} {
    set errortype [hval /sics/$name/errortype]
    set recover [hval /sics/$name/recover]

    for {set i 0} {$i < 8} {incr i} {
	lappend l [expr $i*10 + 5]
    }

    if {$errortype == 5 } {
	if {$recover == 1} {
	    hupdate /sics/$name/errortype 0
	    clientlog "WARNING: regression counter read problem"
        } else {
	    clientlog "ERROR: cannot fix counter problem, aborting"
        }
    }
    return [join $l]
}
#---------------------------------------------------
proc simcounter::status {name} {
    set t [clock seconds]
    set stop  [hval /sics/$name/stopsignal]
    if {$stop == 1} {
	 hsetprop /sics/$name/status geterror  \
		"Stopped"
	return error 
    }

    set istat [hval /sics/$name/internalstatus]
    set errortype [hval /sics/$name/errortype]
    set recover [hval /sics/$name/recover]
    hdelprop /sics/$name/status geterror

    switch $errortype {
	1 -
	3 -
	4 -
	5 -
	0 {
	    if {[string compare $istat paused] == 0} {
		return paused
	    }
	    if {[string compare $istat nobeam] == 0} {
		return nobeam
	    }
	    if {[string compare $istat idle] == 0} {
		return idle
	    }
	    if {[string compare $istat error] == 0} {
		hupdate /sics/$name/stoptime [doubletime]
		hsetprop /sics/$name/status geterror "Simulated counter error"
		return error
	    }
	    hdelprop /sics/$name/status geterror
	    set t [clock seconds]
	    set ctime [hval /sics/$name/stoptime]
	    if {$t > $ctime} {
		set p [hval /sics/$name/preset]
		hupdate /sics/$name/time $p
		hupdate /sics/$name/stoptime [doubletime]
		hupdate /sics/$name/internalstatus idle
		return idle
	    } else {
		hupdate /sics/$name/time [expr $ctime - $t]
		return run
	    }
	}
	2 {
	    hsetprop  /sics/$name/status geterror "Simulated status error, aborting"
	    if {$recover == 1} {
		hupdate /sics/$name/errortype 0
		clientlog "WARNING: simulated status problem while counting"
		return run
            } else {
		hupdate /sics/$name/stoptime [doubletime]
		return error
	    }
	}
	default {
	    hupdate /sics/$name/stoptime [doubletime]
	    hsetprop /sics/$name/status geterror  \
		"Unknown error type $errortype in simcounter::status"
	    return error
	}
    }
    
}
#---------------------------------------------------
proc simcounter::control {name target} {
    set errortype [hval /sics/$name/errortype]
    set recover [hval /sics/$name/recover]
    set preset [hval /sics/$name/preset]

    switch $target {
	1000 {
	    if {$errortype == 1 } {
		if {$recover == 1} {
		    hupdate /sics/$name/errortype 0
		    hupdate /sics/$name/status restart
		    clientlog "WARNING: Failed to start simulation counter"
		    return [simcounter::control $name $target]
                } else {
		    hupdate /sics/$name/internalstatus error
		    hsetprop /sics/$name/status geterror "failed to start counter, aborting"
		    hsetprop /sics/$name/status geterror "Failed to start counter"
		    error "ERROR: failed to start counter, aborting"
                }
            } else {
		hupdate /sics/$name/internalstatus run
		hupdate /sics/$name/status run
		hupdate /sics/$name/stoptime [expr [clock seconds] +$preset]
		hupdate /sics/$name/stopsignal 0
		hupdate /sics/$name/starttime [doubletime]
		if {$errortype == 10} {
		    hupdate /sics/$name/stoptime [expr [clock seconds] + 1]
                }
	    }
	}
	1001 {
	    hupdate /sics/$name/stopsignal 1
	}
	1002 {
	    if {$errortype == 3} {
		clientlog "Failed to pause simulation counter"
		if {$recover == 1} {
		    hupdate /sics/$name/errortype 0
		    hupdate /sics/$name/internalstatus paused
                } else {
		    error "Failed to pause simulation counter"
                }
            } else {
		hupdate /sics/$name/internalstatus paused
            }
	}
	1003 {
	    if {$errortype == 4} {
		clientlog "Failed to continue simulation counter"
		if {$recover == 1} {
		    hupdate /sics/$name/errortype 0
		    hupdate /sics/$name/internalstatus run
                } else {
		    error "Failed to continue simulation counter"
                }
            } else {
		hupdate /sics/$name/internalstatus run
            }
	}
	default {
	    sct print "ERROR: bad start target $target given to control"
	    return idle
	}
    }
    return OK
}
#--------------------------------------------------
proc simcounter::setpar {name par cter val} {
    hupdate /sics/$name/$par $val
}
#--------------------------------------------------
proc simcounter::readcontrol {name}  {
    return [hval /sics/$name/control]
}
#===================================================
proc simcounter::make {name} {
    MakeSecCounter $name 8

    set path /sics/$name/values
    hdel $path
    hfactory /sics/$name/values script "simcounter::values $name" \
	hdbReadOnly intar 8 

    set path /sics/$name/status
    hdel $path
    hfactory $path script "simcounter::status $name" \
	hdbReadOnly text

    set path /sics/${name}/control
    hdel $path
    hfactory $path script "simcounter::readcontrol $name" \
	"simcounter::control $name" int

    hfactory /sics/${name}/thresholdcounter plain mugger int
    hsetprop /sics/${name}/thresholdcounter __save true
    set path /sics/${name}/threshold
    hfactory $path plain mugger float

    $name makescriptfunc gettime "el737gettime $name" spy

    $name makescriptfunc setpar "simcounter::setpar $name" spy
    hfactory /sics/$name/setpar/par plain user text
    hfactory /sics/$name/setpar/counter plain user int
    hfactory /sics/$name/setpar/value plain user float

    hfactory /sics/$name/recover plain user int
    hfactory /sics/$name/errortype plain user int
    hupdate /sics/$name/recover 1
    hupdate /sics/$name/errortype 0

    hfactory /sics/$name/internalstatus plain user text
    hupdate /sics/$name/internalstatus idle

    hsetprop /sics/$name/mode values "timer,monitor"
    haddcheck /sics/$name/mode values

    hfactory /sics/$name/stopsignal plain user int
    hupdate /sics/$name/stopsignal 0

    hfactory /sics/$name/starttime plain internal float
    hsetprop /sics/$name/starttime fmt "%12.9f"
    hfactory /sics/$name/stoptime plain internal float
    hsetprop /sics/$name/stoptime fmt "%12.9f"

}
