#------------------------------------------------------------------
# EPICS motor driver using the new epicsadapter in SICS
#
# Mark Koennecke, November 2014
#-------------------------------------------------------------------

namespace eval epicsmotor {}

#--------------------------------------------------------------------
proc epicsmotor::halt {name} {
	hset /sics/${name}/ehalt 1
}
#--------------------------------------------------------------------
proc epicsmotor::statusscript {name} {
	set dmov [sget "/sics/${name}/dmov"]
	set stat [sget "/sics/${name}/stat"]
	if {$dmov == 0} {
		hupdate /sics/${name}/status run
	} else {
		if {$stat != 0} {
			set txt [epicsadapter convertalarm $stat]
			hupdate /sics/${name}/error $txt
			hupdate /sics/${name}/status error
		} else {
			hupdate /sics/${name}/error none
			hupdate /sics/${name}/status idle			
		}
	}
}
#--------------------------------------------------------------------
proc epicsmotor::makeintern {name pvroot} {
	MakeSecMotor $name

	hfactory /sics/${name}/stat plain internal int
	hfactory /sics/${name}/dmov plain internal int
	hfactory /sics/${name}/ehalt plain user int

	epicsadapter connectread /sics/${name}/stat ${pvroot}.STAT
	epicsadapter connectread /sics/${name}/dmov ${pvroot}.DMOV
	epicsadapter connectread /sics/${name}/hardposition ${pvroot}.DRBV
	epicsadapter connectread /sics/${name}/hardlowerlim ${pvroot}.LLM
	epicsadapter connectread /sics/${name}/hardupperlim ${pvroot}.HLM
	epicsadapter connectread /sics/${name}/targetposition ${pvroot}.DVAL
	epicsadapter connectread /sics/${name}/ehalt ${pvroot}.STOP

	epicsadapter connectwrite /sics/${name}/hardposition ${pvroot}.DVAL
	epicsadapter connectwrite /sics/${name}/ehalt ${pvroot}.STOP

	$name makescriptfunc halt "epicsmotor::halt $name" user
	hscriptnotify /sics/${name}/dmov "epicsmotor::statusscript $name"

}
#-------------------------------------------------------------------------
proc epicsmotor::make {name pvroot} {
    epicsmotor::makeintern $name $pvroot
    hscriptnotify /sics/${name}/dmov "epicsmotor::statusscript $name"
}
#--------------------------------------------------------------------
proc epicsmotor::statusscripttxt {name} {
	set dmov [sget "/sics/${name}/dmov"]
	set stat [sget "/sics/${name}/stat"]
	if {$dmov == 0} {
		hupdate /sics/${name}/status run
	} else {
		if {$stat != 0} {
			set txt [epicsadapter convertalarm $stat]
		        set txt2 [hget /sics/${name}/MsgTxt]
		        if {[string length $txt2] > 1} {
			   hupdate /sics/${name}/error $txt2
		        } else {
			   hupdate /sics/${name}/error $txt
		        }
			hupdate /sics/${name}/status error
		} else {
			hupdate /sics/${name}/error none
			hupdate /sics/${name}/status idle			
		}
	}
}
#--------------------------------------------------------------------
proc epicsmotor::maketxt {name pvroot} {
    epicsmotor::makeintern $name $pvroot
    
    hfactory /sics/${name}/MsgTxt plain internal text
    epicsadapter connectread /sics/${name}/MsgTxt ${pvroot}-MsgTxt
    hscriptnotify /sics/${name}/dmov "epicsmotor::statusscripttxt $name"
    
}
