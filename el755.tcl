#-------------------------------------------------------------
# This is a scriptcontext driver for the PSI EL755 magnet 
# controller.
# 
# scriptchains:
#         read -  readreply
#         write - writereply - writereadback
#
# copyright: see file COPYRIGHT
#
# Mark Koennecke, November 2009
#--------------------------------------------------------------

namespace eval el755 {}

#--------------------------------------------------------------
proc el755::read {num} {
  sct send [format "I %d" $num]
  return readreply
}
#--------------------------------------------------------------
proc el755::readreply {num} {
  set reply [sct result]
  if {[string first ? $reply] >= 0} {
    if {[string first ?OV $reply] >= 0} {
      sct send [format "I %d" $num]
#      clientput "EL755 did an overflow...."
      return readreply
    }
    error $reply
  }
  set n [stscan $reply "%f %f" soll ist]
  if {$n < 2} {
      sct send [format "I %d" $num]
      clientput "Invalid response $reply from EL755"
      return readreply
  }
  sct update $ist
  return idle
}
#------------------------------------------------------------------
proc el755::write {num} {
  set cur [sct target]
  sct send [format "I %d %f" $num $cur]
  return writereply
}
#------------------------------------------------------------------
proc el755::writereply {num} {
  set reply [sct result]
  if {[string first ? $reply] >= 0} {
    if {[string first ?OV $reply] >= 0} {
      set cur [sct target]
      sct send [format "I %d %f" $num $cur]
#      clientput "EL755 did an overflow...."
      return writereply
    }
    error $reply
  }
  sct send [format "I %d" $num]
  return writereadback  
}
#--------------------------------------------------------------------
proc el755::writereadback {num} {
  set reply [sct result]
  if {[string first ? $reply] >= 0} {
    if {[string first ?OV $reply] >= 0} {
      set cur [sct target]
      sct send [format "I %d" $num]
#      clientput "EL755 did an overflow...."
      return writereadback
    }
    error $reply
  }
  set n [stscan $reply "%f %f" soll ist]
  if {$n < 2} {
      sct send [format "I %d" $num]
      clientput "Invalid response $reply from EL755"
      return writereadback
  }
  set cur [sct target]
  if {abs($cur - $soll) < .1} {
    return idle
  }
  return el755::write $num
}
#--------------------------------------------------------------------
proc el755::makeel755 {name num sct} {
  stddrive::makestddrive $name EL755Magnet $sct
  set path /sics/${name}
  hsetprop $path read el755::read $num
  hsetprop $path readreply el755::readreply $num
  hsetprop $path write el755::write $num
  hsetprop $path writereply el755::writereply $num
  hsetprop $path writereadback el755::writereadback $num
# poll and write have already been set by stddrive
}
