#-------------------------------------------------------------------
# This is a scriptcontext driven driver for the Advantech ADAM-6051 
# TCP/IP Digital I/O module. This module has 12 Digital Inputs and 
# 2 digital outputs. This driver uses the modbus port of the device
#
# copyright: see file COPYRIGHT
#
# Mark Koennecke, May 2014
#
# Added flipping out bits by scriptchain
# Mark Koennecke, July 2014
#-------------------------------------------------------------------

namespace eval adam {}

#-------------------------------------------------------------------
proc adam::read {} {
    sct send "1 0 0 0 0 6 1 1 0 0 0 12 / skip9 int int"
    return adamread
}
#-------------------------------------------------------------------
proc adam::adamread {name} {
    set result [sct result]
    if {[string first err $result] >= 0} {
	sct print "ERROR: on adam-6051: $result"
    } else {
	stscan $result "%d %d" low high
	for {set i 0} {$i < 8} {incr i} {
	    if { $low & (1 << $i)} {
		hupdate /sics/$name/in${i} 1
	    } else {
		hupdate /sics/$name/in${i} 0
            }
        }
	for {set i 0} {$i < 4} {incr i} {
	    set io [expr 8 + $i]
	    set io in${io}
	    if { $high & (1 << $i)} {
		hupdate /sics/$name/$io 1
	    } else {
		hupdate /sics/$name/$io 0
            }
        }
	hdelprop /sics/${name}/in1 geterror
    }
    return idle
} 
#--------------------------------------------------------------------
proc adam::write {address} {
    set t [sct target]
    if {$t == 1} {
	set message "2 0 0 0 0 6 1 5 0 $address 255 0 / skip7 int skip2 int int"
    } else {
	set message "2 0 0 0 0 6 1 5 0 $address 0 0 / skip7 int skip2 int int"
    }
    sct send $message
    return writeresponse
}
#--------------------------------------------------------------------
proc adam::writeresponse {name} {
   set result [sct result]
    if {[string first err $result] >= 0} {
	sct print "ERROR: on adam-6051: $result"
    } 
    stscan $result "%d %d %d" code hi lo
    if {$code > 80} {
	sct print "Device error setting DO"
    }
    [sct controller] queue /sics/$name/out0 progress read
    return idle
} 
#-----------------------------------------------------------------
proc adam::fwrite {address} {
    set t [sct target]
    set message "2 0 0 0 0 6 1 5 0 $address 0 0 / skip7 int skip2 int int"
    sct send $message
    return f1response
}
#------------------------------------------------------------------
proc adam::f1response {address} {
    set message "2 0 0 0 0 6 1 5 0 $address 255 0 / skip7 int skip2 int int"
    sct send $message
    return f2response
}
#------------------------------------------------------------------
proc adam::f2response {address} {
    after 100
    set message "2 0 0 0 0 6 1 5 0 $address 0 0 / skip7 int skip2 int int"
    sct send $message
    return writeresponse
}
#-------------------------------------------------------------------
proc adam::readout {} {
    sct send "3 0 0 0 0 6 1 1 0 16 0 2 / skip9 int"
    return adamoutread
}
#-------------------------------------------------------------------
proc adam::adamoutread {name} {
    set result [sct result]
    if {[string first err $result] >= 0} {
	sct print "ERROR: on adam-6051: $result"
    } else {
	for {set i 0} {$i < 2} {incr i} {
	    if { $result & (1 << $i)} {
		hupdate /sics/$name/out${i} 1
	    } else {
		hupdate /sics/$name/out${i} 0
            }
        }
    }
    return idle
} 
#--------------------------------------------------------------------
proc adam::update {name} {
    ${name}sct processnode /sics/$name/in1
    return Done
}
#--------------------------------------------------------------------
proc adam::make {name hostport} {
    makesctcontroller ${name}sct bin $hostport
    MakeSicsObj $name DIO
    for {set i 0} {$i < 12} {incr i} {
	hfactory /sics/$name/in${i} plain internal int
    }
    hsetprop /sics/$name/in1 read adam::read
    hsetprop /sics/$name/in1 adamread "adam::adamread $name"
    ${name}sct poll /sics/$name/in1 20

    $name makescriptfunc update "adam::update $name" spy

    hfactory /sics/$name/out0 plain user int
    hsetprop /sics/$name/out0 write "adam::write 16"
    hsetprop /sics/$name/out0 writeresponse "adam::writeresponse $name"
    ${name}sct write /sics/$name/out0

    hfactory /sics/$name/out1 plain user int
    hsetprop /sics/$name/out1 write "adam::write 17"
    hsetprop /sics/$name/out1 writeresponse "adam::writeresponse $name"
    ${name}sct write /sics/$name/out1

    hsetprop /sics/$name/out0 read adam::readout
    hsetprop /sics/$name/out0 adamoutread "adam::adamoutread $name"
    ${name}sct poll /sics/$name/out0 20

} 
#------------------------------------------------------------------
proc adam::configureflip {name no} {
    if {$no == 0} {
	hsetprop /sics/$name/out0 write "adam::fwrite 16"
	hsetprop /sics/$name/out0 f1response "adam::f1response 16"
	hsetprop /sics/$name/out0 f2response "adam::f2response 16"
	hsetprop /sics/$name/out0 writeresponse "adam::writeresponse $name"
    } else {
	hsetprop /sics/$name/out1 write "adam::fwrite 17"
	hsetprop /sics/$name/out1 f1response "adam::f1response 17"
	hsetprop /sics/$name/out1 f2response "adam::f2response 17"
	hsetprop /sics/$name/out1 writeresponse "adam::writeresponse $name"
    }
}
