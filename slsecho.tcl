#--------------------------------------------------------------
# This is a scriptcontext based driver for the SLS magnet 
# controllers interfaced via the new shiny, silvery TCP/IP
# interface box. 
#
# Mark Koennecke, March 2010
#
# As this already contained the error code mapping, it has 
# been expanded to handle the vmemaggi version of the SLS
# magnet intrface too
#
# Mark Koennecke, July 2017 
#---------------------------------------------------------------
namespace eval slsecho {}


proc slsecho::sendread {num} {
	sct send "$num:r:0x9c:0:read"
	return readreply
}
#---------------------------------------------------------------
proc slsecho::readreply {} {
	set reply [sct result]
	set l [split $reply :]
#	set v [lindex $l 1]
#	clientput "Received $reply, val = $v"
	sct update [lindex $l 1]
	return idle
}
#--------------------------------------------------------------
proc slsecho::sendwrite {num} {
	set val [sct target]
	hupdate [sct]/target [sct target]
	hupdate [sct]/stop 0
#	sct send "$num:w:0x90:$val:write"
	sct send "$num:s:0x9c:$val:write"
	return read
}
#--------------------------------------------------------------
proc slsecho::writereply {} {
	set path [sct]
	set root [file dirname $path]
	[sct controller] queue $root/error progress read
	return idle
}
#--------------------------------------------------------------
proc slsecho::readupper {num} {
	sct send "$num:r:0x76:0:read"
	return readreply
}
#--------------------------------------------------------------
proc slsecho::readlower {num} {
	sct send "$num:r:0x77:0:read"
	return readreply	
}
#--------------------------------------------------------------
proc slsecho::readonoff {num} {
	sct send "$num:r:0x24:0:none"
	return onoffreply	
}
#---------------------------------------------------------------
proc slsecho::onoffreply {} {
	set reply [sct result]
	set l [split $reply :]
	set val [lindex $l 1]
	if {$val == 1} {
		sct update on
	} else {
		sct update off
	}
	return idle
}
#---------------------------------------------------------------
proc slsecho::writeonoff {num} {
	set val [sct target]
	if {[string compare $val on] == 0} {
		set val 1
	} elseif {[string compare $val off] == 0} {
		set val 0
	} else {
		clientput "ERROR: Invalid target $val requested, only on/off"
		return idle
	}
	sct send "$num:w:0x3c:$val:none"
	[sct controller] queue [sct] progress read
	return writereply		
}
#--------------------------------------------------------------
proc slsecho::readerror {num} {
	sct send "$num:r:0x29:0:none"
	return errorreply	
}
#--------------------------------------------------------------
proc slsecho::errorreply {} {
	global slsecho::error
	set reply [sct result]
	set l [split $reply :]
	set val [lindex $l 1]
	set key [format "0x%x" [expr int($val)]]
        if { int($val) != 0} {
        	clientput "$slsecho::error($key)"
        }
	sct update $slsecho::error($key)
	return idle
}
#---------------------------------------------------------------
proc slsecho::makeslsecho {name num sct} {
	makesctdriveobj $name float user SLSEchoMagnet $sct
  	hfactory /sics/${name}/tolerance plain internal float
  	hset /sics/${name}/tolerance .1
  	hfactory /sics/${name}/upperlimit plain internal float
	hset /sics/${name}/upperlimit 10
	hfactory /sics/${name}/lowerlimit plain internal float
	hset /sics/${name}/lowerlimit -10
	hfactory /sics/${name}/stop plain user int
  	hset /sics/${name}/stop 0
  
	hfactory /sics/${name}/target plain internal float
	hupdate /sics/${name}/target 0
	hsetprop /sics/${name}/target __save true

  	hsetprop /sics/${name} checklimits stddrive::stdcheck $name
  	hsetprop /sics/${name} checkstatus stddrive::stdstatus $name
  	hsetprop /sics/${name} halt stddrive::stop $name
	
	set path /sics/${name}
	hsetprop $path read slsecho::sendread $num
	hsetprop $path readreply slsecho::readreply
	$sct poll $path 10
	hsetprop $path write slsecho::sendwrite $num
	hsetprop $path writereply slsecho::writereply
	$sct write $path
	
	hsetprop /sics/${name}/upperlimit read slsecho::readupper $num	
	hsetprop /sics/${name}/upperlimit readreply slsecho::readreply
	$sct poll /sics/${name}/upperlimit  60

	hsetprop /sics/${name}/lowerlimit read slsecho::readlower $num	
	hsetprop /sics/${name}/lowerlimit readreply slsecho::readreply
	$sct poll /sics/${name}/lowerlimit  60

	hfactory /sics/${name}/onoff plain user text
	hsetprop /sics/${name}/onoff read slsecho::readonoff $num	
	hsetprop /sics/${name}/onoff onoffreply slsecho::onoffreply
	$sct poll /sics/${name}/onoff  60
	hsetprop /sics/${name}/onoff write slsecho::writeonoff $num	
	hsetprop /sics/${name}/onoff writereply slsecho::writereply
	$sct write /sics/${name}/onoff
	
	hfactory /sics/${name}/error plain internal text
	hsetprop /sics/${name}/error read slsecho::readerror $num	
	hsetprop /sics/${name}/error errorreply slsecho::errorreply
	$sct poll /sics/${name}/error  10
	
#----------------- update everything	
	hset /sics/${name}/onoff on
	$sct queue /sics/${name} progress read		
	$sct queue /sics/${name}/upperlimit progress read		
	$sct queue /sics/${name}/lowerlimit progress read	
	$sct queue /sics/${name}/onoff progress read
	$sct queue /sics/${name}/error progress read
}
#-----------------------------------------------------------------------------------------------
# The code for the vmemaggi style interface at AMOR
#-----------------------------------------------------------------------------------------------
proc slsecho::maggiread {num par} {
    sct send "r $num $par"
    return readreply
}
#-----------------------------------------------------------------------------------------------
proc slsecho::maggidata {} {
    set reply [sct result]
    if {[string first ERROR $reply] >= 0} {
	set l [split $reply :]
	error [lindex $l 2]
    }
    return $reply
}
#------------------------------------------------------------------------------------------------
proc slsecho::maggireply {} {
    set status [catch {slsecho::maggidata} data]
    if {$status != 0} {
	sct geterror $data
	error $data
    }
    set l [split $data]
    hdelprop [sct] geterror 
    sct update [lindex $l 2]
    return idle
}
#------------------------------------------------------------------------------------------------
proc slsecho::maggierrorreply {} {
    global slsecho::error
    set status [catch {slsecho::maggidata} data]
    if {$status != 0} {
	sct geterror $data
	error $data
    }
    set l [split $data]
   
    set val [lindex $l 2]
    set key [format "0x%x" [expr int($val)]]
    if {int($val) != 0} {
	clientlog "$slsecho::error($key)"
    }
    sct update $slsecho::error($key)
    return idle
}
#------------------------------------------------------------------------------------------------
proc slsecho::maggiwrite {num par} {
    set val [sct target]
    sct send "w $num $par $val"
    return writereply
}
#------------------------------------------------------------------------------------------------
proc slsecho::maggiwriteonoff {num} {
    set val [sct target]
    if {[string first on $val] >= 0} {
	sct send "w $num on"
    } elseif {[string first off $val] >= 0}  {
	sct send "w $num off"
    } else {
	error "Wrong value $target, only on or off allowed"
    }
    return writereply
}
#------------------------------------------------------------------------------------------------
proc slsecho::maggiwritereply {num par} {
    return [slsecho::maggiread $num $par]
}
#------------------------------------------------------------------------------------------------
proc slsecho::makemaggi {name num sct} {
	makesctdriveobj $name float user SLSMaggiMagnet $sct
  	hfactory /sics/${name}/tolerance plain internal float
  	hset /sics/${name}/tolerance .1
  	hfactory /sics/${name}/upperlimit plain internal float
	hset /sics/${name}/upperlimit 10
	hfactory /sics/${name}/lowerlimit plain internal float
	hset /sics/${name}/lowerlimit -10
	hfactory /sics/${name}/stop plain user int
  	hset /sics/${name}/stop 0
  
	hfactory /sics/${name}/target plain internal float
	hupdate /sics/${name}/target 0
	hsetprop /sics/${name}/target __save true

  	hsetprop /sics/${name} checklimits stddrive::stdcheck $name
  	hsetprop /sics/${name} checkstatus stddrive::stdstatus $name
  	hsetprop /sics/${name} halt stddrive::stop $name
	
	set path /sics/${name}
	hsetprop $path read slsecho::maggiread $num cur
	hsetprop $path readreply slsecho::maggireply
	$sct poll $path 10
	hsetprop $path write slsecho::maggiwrite $num cur
	hsetprop $path writereply slsecho::maggiwritereply $num cur
	$sct write $path
	
	hsetprop /sics/${name}/upperlimit read slsecho::maggiread $num hl	
	hsetprop /sics/${name}/upperlimit readreply slsecho::maggireply
	$sct poll /sics/${name}/upperlimit  60

	hsetprop /sics/${name}/lowerlimit read slsecho::maggiread $num ll	
	hsetprop /sics/${name}/lowerlimit readreply slsecho::maggireply
	$sct poll /sics/${name}/lowerlimit  60

	hfactory /sics/${name}/onoff plain user text
	hsetprop /sics/${name}/onoff read slsecho::maggiread $num onoff	
	hsetprop /sics/${name}/onoff readreply slsecho::maggireply
	$sct poll /sics/${name}/onoff  60
	hsetprop /sics/${name}/onoff write slsecho::maggiwriteonoff $num	
	hsetprop /sics/${name}/onoff writereply slsecho::maggiwritereply $num onoff
	$sct write /sics/${name}/onoff
	
	hfactory /sics/${name}/error plain internal text
	hsetprop /sics/${name}/error read slsecho::maggiread $num err	
	hsetprop /sics/${name}/error readreply slsecho::maggierrorreply
	$sct poll /sics/${name}/error  10
	
#----------------- update everything	
	hset /sics/${name}/onoff on
	$sct queue /sics/${name} progress read		
	$sct queue /sics/${name}/upperlimit progress read		
	$sct queue /sics/${name}/lowerlimit progress read	
	$sct queue /sics/${name}/onoff progress read
	$sct queue /sics/${name}/error progress read
}

#------------------------------------------------------------------------------------------------
#                 error codes
#-------------------------------------------------------------------------------------------------
 set slsecho::error(0x0) "NO" 
 set slsecho::error(0x1)  "DEVICE_STATE_ERROR" 
 set slsecho::error(0x2) "DEVICE_SUPERVISOR_DISABLED" 
 set slsecho::error(0x3) "COMMAND_ABORT" 
 set slsecho::error(0x4) "DATA_NOT_STORED" 
 set slsecho::error(0x5) "ERROR_ERASING_FLASH" 
 set slsecho::error(0x6) "COMMUNICATION_BREAK" 
 set slsecho::error(0x7) "INTERNAL_COMMUNICATION_ERROR" 
 set slsecho::error(0x8) "MASTER_CARD_ERROR" 
 set slsecho::error(0x9) "INTERNAL_BUFFER_FULL" 
 set slsecho::error(0xa) "WRONG_SECTOR" 
 set slsecho::error(0xb) "DATA_NOT_COPIED" 
 set slsecho::error(0xc) "WRONG_DOWNLOAD_PARAMETERS" 
 set slsecho::error(0xd) "DEVICE_PARAMETRIZATION_ERROR" 
 set slsecho::error(0x10) "TIMEOUT_DC_LINK_VOLTAGE" 
 set slsecho::error(0x11) "TIMEOUT_AUXILIARY_RELAY_ON" 
 set slsecho::error(0x12) "TIMEOUT_AUXILIARY_RELAY_OFF" 
 set slsecho::error(0x13) "TIMEOUT_MAIN_RELAY_ON" 
 set slsecho::error(0x14) "TIMEOUT_MAIN_RELAY_OFF" 
 set slsecho::error(0x15) "TIMEOUT_DATA_DOWNLOAD" 
 set slsecho::error(0x20) "INTERLOCK" 
 set slsecho::error(0x21) "MASTER_SWITCH" 
 set slsecho::error(0x22) "MAGNET_INTERLOCK" 
 set slsecho::error(0x23) "TEMPERATURE_TRANSFORMER" 
 set slsecho::error(0x24) "TEMPERATURE_RECTIFIER" 
 set slsecho::error(0x25) "TEMPERATURE_CONVERTER" 
 set slsecho::error(0x26) "CURRENT_TRANSDUCER" 
 set slsecho::error(0x27) "TEMPERATURE_POLARITY_SWITCH" 
 set slsecho::error(0x28) "POWER_SEMICONDUCTOR" 
 set slsecho::error(0x29) "MAIN_RELAY" 
 set slsecho::error(0x2a) "AD_CONVERTER_CARD" 
 set slsecho::error(0x2b) "POLARITY_SWITCH" 
 set slsecho::error(0x2c) "AUXILIARY_RELAY" 
 set slsecho::error(0x2d) "MASTER_SWITCH_T1" 
 set slsecho::error(0x2e) "MASTER_SWITCH_T2" 
 set slsecho::error(0x2f) "TEMPERATURE_MAGNET" 
 set slsecho::error(0x30) "WATER_MAGNET" 
 set slsecho::error(0x31) "WATER_RACK" 
 set slsecho::error(0x40) "LOAD_CURRENT_TOO_HIGH" 
 set slsecho::error(0x41) "DC_LINK_VOLTAGE_TOO_LOW" 
 set slsecho::error(0x42) "DC_LINK_VOLTAGE_TOO_HIGH" 
 set slsecho::error(0x43) "LOAD_VOLTAGE_TOO_HIGH" 
 set slsecho::error(0x44) "LOAD_CURRENT_RIPPLE_TOO_HIGH" 
 set slsecho::error(0x45) "DC_LINK_ISOLATION_NOT_OK" 
 set slsecho::error(0x46) "LOAD_ISOLATION_NOT_OK" 
 set slsecho::error(0x47) "LOAD_IMPEDANCE_OUT_OF_RANGE" 
 set slsecho::error(0x48) "SHUT_OFF_CURRENT_TOO_HIGH" 
 set slsecho::error(0x49) "LOAD_DC_CURRENT_TOO_HIGH" 
 set slsecho::error(0x4a) "CURRENT_I1A1_TOO_HIGH" 
 set slsecho::error(0x4b) "CURRENT_I1B1_TOO_HIGH" 
 set slsecho::error(0x4c) "CURRENT_I1A2_TOO_HIGH" 
 set slsecho::error(0x4d) "CURRENT_I1B2_TOO_HIGH" 
 set slsecho::error(0x4e) "CURRENT_I2A1_TOO_HIGH" 
 set slsecho::error(0x4f) "CURRENT_I2B1_TOO_HIGH" 
 set slsecho::error(0x50) "CURRENT_I2A2_TOO_HIGH" 
 set slsecho::error(0x51) "CURRENT_I2B2_TOO_HIGH" 
 set slsecho::error(0x52) "CURRENT_I3P_TOO_HIGH" 
 set slsecho::error(0x53) "CURRENT_I3N_TOO_HIGH" 
 set slsecho::error(0x54) "CURRENT_IE_TOO_HIGH" 
 set slsecho::error(0x55) "VOLTAGE_U1A_TOO_LOW" 
 set slsecho::error(0x56) "VOLTAGE_U1B_TOO_LOW" 
 set slsecho::error(0x57) "DIFF_CURRENT_I1A1_I1A2_TOO_HIGH" 
 set slsecho::error(0x58) "DIFF_CURRENT_I1B1_I1B2_TOO_HIGH" 
 set slsecho::error(0x59) "DIFF_CURRENT_I2A1_I2A2_TOO_HIGH" 
 set slsecho::error(0x5a) "DIFF_CURRENT_I2B1_I2B2_TOO_HIGH" 
 set slsecho::error(0x5b) "DIFF_CURRENT_I3P_I3N_TOO_HIGH" 
 set slsecho::error(0x5c) "CURRENT_I1A_TOO_HIGH" 
 set slsecho::error(0x5d) "CURRENT_I1B_TOO_HIGH" 
 set slsecho::error(0x5e) "CURRENT_I3A1_TOO_HIGH" 
 set slsecho::error(0x5f) "CURRENT_I3B1_TOO_HIGH" 
 set slsecho::error(0x60) "CURRENT_I3A2_TOO_HIGH" 
 set slsecho::error(0x61) "CURRENT_I3B2_TOO_HIGH" 
 set slsecho::error(0x62) "CURRENT_I4_TOO_HIGH" 
 set slsecho::error(0x63) "CURRENT_I5_TOO_HIGH" 
 set slsecho::error(0x64) "DIFF_CURRENT_I3A1_I3A2_TOO_HIGH" 
 set slsecho::error(0x65) "DIFF_CURRENT_I3B1_I3B2_TOO_HIGH" 
 set slsecho::error(0x66) "DIFF_CURRENT_I4_I5_TOO_HIGH" 
 set slsecho::error(0x67) "VOLTAGE_U3A_TOO_LOW" 
 set slsecho::error(0x68) "VOLTAGE_U3B_TOO_LOW" 
 set slsecho::error(0x69) "VOLTAGE_U1_TOO_LOW" 
 set slsecho::error(0x6a) "VOLTAGE_U3A_TOO_HIGH" 
 set slsecho::error(0x6b) "VOLTAGE_U3B_TOO_HIGH" 
 set slsecho::error(0x6c) "SPEED_ERROR_TOO_HIGH" 
 set slsecho::error(0x70) "MAIN_RELAY_A" 
 set slsecho::error(0x71) "MAIN_RELAY_B" 
 set slsecho::error(0x72) "POWER_SWITCH_A" 
 set slsecho::error(0x73) "POWER_SWITCH_B" 
 set slsecho::error(0x74) "MONITOR_TRAFO_A" 
 set slsecho::error(0x75) "MONITOR_TRAFO_B" 
 set slsecho::error(0x76) "TEMPERATURE_RECTIFIER_A" 
 set slsecho::error(0x77) "TEMPERATURE_RECTIFIER_B" 
 set slsecho::error(0x78) "TEMPERATURE_CONVERTER_A" 
 set slsecho::error(0x79) "TEMPERATURE_CONVERTER_B" 
 set slsecho::error(0x7a) "TEMPERATURE_CONVERTER_A1" 
 set slsecho::error(0x7b) "TEMPERATURE_CONVERTER_B1" 
 set slsecho::error(0x7c) "TEMPERATURE_CONVERTER_A2" 
 set slsecho::error(0x7d) "TEMPERATURE_CONVERTER_B2" 
 set slsecho::error(0x7e) "TEMPERATURE_TRANSFORMER_A" 
 set slsecho::error(0x7f) "TEMPERATURE_TRANSFORMER_B" 
 set slsecho::error(0x80) "WATER_RECTIFIER_A" 
 set slsecho::error(0x81) "WATER_RECTIFIER_B" 
 set slsecho::error(0x82) "WATER_CONVERTER_A" 
 set slsecho::error(0x83) "WATER_CONVERTER_B" 
 set slsecho::error(0x84) "WATER_CONVERTER_A1" 
 set slsecho::error(0x85) "WATER_CONVERTER_B1" 
 set slsecho::error(0x86) "WATER_CONVERTER_A2" 
 set slsecho::error(0x87) "WATER_CONVERTER_B2" 
 set slsecho::error(0x88) "WATER_TRANSFORMER_A" 
 set slsecho::error(0x89) "WATER_TRANSFORMER_B" 
 set slsecho::error(0x8a) "DOOR_A" 
 set slsecho::error(0x8b) "DOOR_B" 
 set slsecho::error(0x8c) "DOOR_C" 
 set slsecho::error(0x8d) "POWER_SEMICONDUCTOR_CONVERTER_A" 
 set slsecho::error(0x8e) "POWER_SEMICONDUCTOR_CONVERTER_B" 
 set slsecho::error(0x8f) "POWER_SEMICONDUCTOR_CONVERTER_A1" 
 set slsecho::error(0x90) "POWER_SEMICONDUCTOR_CONVERTER_B1" 
 set slsecho::error(0x91) "POWER_SEMICONDUCTOR_CONVERTER_A2" 
 set slsecho::error(0x92) "POWER_SEMICONDUCTOR_CONVERTER_B2" 
 set slsecho::error(0x93) "CURRENT_TRANSDUCER_I3P" 
 set slsecho::error(0x94) "CURRENT_TRANSDUCER_I3N" 
 set slsecho::error(0x95) "MAGNET_INTERLOCK_1" 
 set slsecho::error(0x96) "MAGNET_INTERLOCK_2" 
 set slsecho::error(0x97) "VENTILATOR" 
 set slsecho::error(0x98) "EMERGENCY_SWITCH" 
 set slsecho::error(0x99) "CAPACITOR_DISCHARGE_A_ON" 
 set slsecho::error(0x9a) "CAPACITOR_DISCHARGE_B_ON" 
 set slsecho::error(0x9b) "CURRENT_TRANSDUCER_I4" 
 set slsecho::error(0x9c) "CURRENT_TRANSDUCER_I5" 
 set slsecho::error(0xb0) "TIMEOUT_DC_LINK_VOLTAGE_PART_A" 
 set slsecho::error(0xb1) "TIMEOUT_DC_LINK_VOLTAGE_PART_B" 
 set slsecho::error(0xb2) "TIMEOUT_AUXILIARY_RELAY_A_ON" 
 set slsecho::error(0xb3) "TIMEOUT_AUXILIARY_RELAY_B_ON" 
 set slsecho::error(0xb4) "TIMEOUT_AUXILIARY_RELAY_A_OFF" 
 set slsecho::error(0xb5) "TIMEOUT_AUXILIARY_RELAY_B_OFF" 
 set slsecho::error(0xb6) "TIMEOUT_MAIN_RELAY_A_ON" 
 set slsecho::error(0xb7) "TIMEOUT_MAIN_RELAY_B_ON" 
 set slsecho::error(0xb8) "TIMEOUT_MAIN_RELAY_A_OFF" 
 set slsecho::error(0xb9) "TIMEOUT_MAIN_RELAY_B_OFF" 
 
